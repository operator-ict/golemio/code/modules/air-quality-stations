--rename tables
ALTER TABLE airqualitystations_indexes RENAME TO airqualitystations_indexes_old;
ALTER TABLE airqualitystations_measurements RENAME TO airqualitystations_measurements_old;

--rename partitions
ALTER TABLE airqualitystations_measurements_min RENAME TO airqualitystations_measurements_min_old;
ALTER TABLE airqualitystations_measurements_y2020m05 RENAME TO airqualitystations_measurements_y2020m05_old;
ALTER TABLE airqualitystations_measurements_y2020m06 RENAME TO airqualitystations_measurements_y2020m06_old;
ALTER TABLE airqualitystations_measurements_y2020m07 RENAME TO airqualitystations_measurements_y2020m07_old;
ALTER TABLE airqualitystations_measurements_y2020m08 RENAME TO airqualitystations_measurements_y2020m08_old;
ALTER TABLE airqualitystations_measurements_y2020m09 RENAME TO airqualitystations_measurements_y2020m09_old;
ALTER TABLE airqualitystations_measurements_y2020m10 RENAME TO airqualitystations_measurements_y2020m10_old;
ALTER TABLE airqualitystations_measurements_y2020m11 RENAME TO airqualitystations_measurements_y2020m11_old;
ALTER TABLE airqualitystations_measurements_y2020m12 RENAME TO airqualitystations_measurements_y2020m12_old;

ALTER TABLE airqualitystations_measurements_y2021m01 RENAME TO airqualitystations_measurements_y2021m01_old;
ALTER TABLE airqualitystations_measurements_y2021m02 RENAME TO airqualitystations_measurements_y2021m02_old;
ALTER TABLE airqualitystations_measurements_y2021m03 RENAME TO airqualitystations_measurements_y2021m03_old;
ALTER TABLE airqualitystations_measurements_y2021m04 RENAME TO airqualitystations_measurements_y2021m04_old;
ALTER TABLE airqualitystations_measurements_y2021m05 RENAME TO airqualitystations_measurements_y2021m05_old;
ALTER TABLE airqualitystations_measurements_y2021m06 RENAME TO airqualitystations_measurements_y2021m06_old;
ALTER TABLE airqualitystations_measurements_y2021m07 RENAME TO airqualitystations_measurements_y2021m07_old;
ALTER TABLE airqualitystations_measurements_y2021m08 RENAME TO airqualitystations_measurements_y2021m08_old;
ALTER TABLE airqualitystations_measurements_y2021m09 RENAME TO airqualitystations_measurements_y2021m09_old;
ALTER TABLE airqualitystations_measurements_y2021m10 RENAME TO airqualitystations_measurements_y2021m10_old;
ALTER TABLE airqualitystations_measurements_y2021m11 RENAME TO airqualitystations_measurements_y2021m11_old;
ALTER TABLE airqualitystations_measurements_y2021m12 RENAME TO airqualitystations_measurements_y2021m12_old;

ALTER TABLE airqualitystations_measurements_y2022m01 RENAME TO airqualitystations_measurements_y2022m01_old;
ALTER TABLE airqualitystations_measurements_y2022m02 RENAME TO airqualitystations_measurements_y2022m02_old;
ALTER TABLE airqualitystations_measurements_y2022m03 RENAME TO airqualitystations_measurements_y2022m03_old;
ALTER TABLE airqualitystations_measurements_y2022m04 RENAME TO airqualitystations_measurements_y2022m04_old;
ALTER TABLE airqualitystations_measurements_y2022m05 RENAME TO airqualitystations_measurements_y2022m05_old;
ALTER TABLE airqualitystations_measurements_y2022m06 RENAME TO airqualitystations_measurements_y2022m06_old;
ALTER TABLE airqualitystations_measurements_y2022m07 RENAME TO airqualitystations_measurements_y2022m07_old;
ALTER TABLE airqualitystations_measurements_y2022m08 RENAME TO airqualitystations_measurements_y2022m08_old;
ALTER TABLE airqualitystations_measurements_y2022m09 RENAME TO airqualitystations_measurements_y2022m09_old;
ALTER TABLE airqualitystations_measurements_y2022m10 RENAME TO airqualitystations_measurements_y2022m10_old;
ALTER TABLE airqualitystations_measurements_y2022m11 RENAME TO airqualitystations_measurements_y2022m11_old;
ALTER TABLE airqualitystations_measurements_y2022m12 RENAME TO airqualitystations_measurements_y2022m12_old;

ALTER TABLE airqualitystations_measurements_y2023m01 RENAME TO airqualitystations_measurements_y2023m01_old;
ALTER TABLE airqualitystations_measurements_y2023m02 RENAME TO airqualitystations_measurements_y2023m02_old;
ALTER TABLE airqualitystations_measurements_y2023m03 RENAME TO airqualitystations_measurements_y2023m03_old;
ALTER TABLE airqualitystations_measurements_y2023m04 RENAME TO airqualitystations_measurements_y2023m04_old;
ALTER TABLE airqualitystations_measurements_y2023m05 RENAME TO airqualitystations_measurements_y2023m05_old;
ALTER TABLE airqualitystations_measurements_y2023m06 RENAME TO airqualitystations_measurements_y2023m06_old;
ALTER TABLE airqualitystations_measurements_y2023m07 RENAME TO airqualitystations_measurements_y2023m07_old;
ALTER TABLE airqualitystations_measurements_y2023m08 RENAME TO airqualitystations_measurements_y2023m08_old;
ALTER TABLE airqualitystations_measurements_y2023m09 RENAME TO airqualitystations_measurements_y2023m09_old;
ALTER TABLE airqualitystations_measurements_y2023m10 RENAME TO airqualitystations_measurements_y2023m10_old;
ALTER TABLE airqualitystations_measurements_y2023m11 RENAME TO airqualitystations_measurements_y2023m11_old;
ALTER TABLE airqualitystations_measurements_y2023m12 RENAME TO airqualitystations_measurements_y2023m12_old;

ALTER TABLE airqualitystations_measurements_y2024m01 RENAME TO airqualitystations_measurements_y2024m01_old;
ALTER TABLE airqualitystations_measurements_y2024m02 RENAME TO airqualitystations_measurements_y2024m02_old;
ALTER TABLE airqualitystations_measurements_y2024m03 RENAME TO airqualitystations_measurements_y2024m03_old;
ALTER TABLE airqualitystations_measurements_y2024m04 RENAME TO airqualitystations_measurements_y2024m04_old;
ALTER TABLE airqualitystations_measurements_y2024m05 RENAME TO airqualitystations_measurements_y2024m05_old;
ALTER TABLE airqualitystations_measurements_y2024m06 RENAME TO airqualitystations_measurements_y2024m06_old;
ALTER TABLE airqualitystations_measurements_y2024m07 RENAME TO airqualitystations_measurements_y2024m07_old;
ALTER TABLE airqualitystations_measurements_y2024m08 RENAME TO airqualitystations_measurements_y2024m08_old;
ALTER TABLE airqualitystations_measurements_y2024m09 RENAME TO airqualitystations_measurements_y2024m09_old;
ALTER TABLE airqualitystations_measurements_y2024m10 RENAME TO airqualitystations_measurements_y2024m10_old;
ALTER TABLE airqualitystations_measurements_y2024m11 RENAME TO airqualitystations_measurements_y2024m11_old;
ALTER TABLE airqualitystations_measurements_y2024m12 RENAME TO airqualitystations_measurements_y2024m12_old;

ALTER TABLE airqualitystations_measurements_y2025m01 RENAME TO airqualitystations_measurements_y2025m01_old;
ALTER TABLE airqualitystations_measurements_y2025m02 RENAME TO airqualitystations_measurements_y2025m02_old;
ALTER TABLE airqualitystations_measurements_y2025m03 RENAME TO airqualitystations_measurements_y2025m03_old;
ALTER TABLE airqualitystations_measurements_y2025m04 RENAME TO airqualitystations_measurements_y2025m04_old;
ALTER TABLE airqualitystations_measurements_y2025m05 RENAME TO airqualitystations_measurements_y2025m05_old;
ALTER TABLE airqualitystations_measurements_y2025m06 RENAME TO airqualitystations_measurements_y2025m06_old;
ALTER TABLE airqualitystations_measurements_y2025m07 RENAME TO airqualitystations_measurements_y2025m07_old;
ALTER TABLE airqualitystations_measurements_y2025m08 RENAME TO airqualitystations_measurements_y2025m08_old;
ALTER TABLE airqualitystations_measurements_y2025m09 RENAME TO airqualitystations_measurements_y2025m09_old;
ALTER TABLE airqualitystations_measurements_y2025m10 RENAME TO airqualitystations_measurements_y2025m10_old;
ALTER TABLE airqualitystations_measurements_y2025m11 RENAME TO airqualitystations_measurements_y2025m11_old;
ALTER TABLE airqualitystations_measurements_y2025m12 RENAME TO airqualitystations_measurements_y2025m12_old;

ALTER TABLE airqualitystations_measurements_max RENAME TO airqualitystations_measurements_max_old;

CREATE TABLE airqualitystations_indexes (
    "station_id" varchar(255) NOT NULL, -- airqualitystations.id, e.g. CZ_A_AKALA
    "measured_from" timestamptz NOT NULL, -- States[].DateFromUTC, e.g. 159242949294
    "measured_to" timestamptz NOT NULL, -- States[].DateToUTC, e.g. 159244949294
    "index_code" varchar(50) NOT NULL, -- airqualitystations_metadata_indexes.index_code, e.g. 1A

    "create_batch_id" bigint,
    "created_at" timestamp with time zone,
    "created_by" character varying(150),
    "update_batch_id" bigint,
    "updated_at" timestamp with time zone,
    "updated_by" character varying(150),
    CONSTRAINT airqualitystations_indexes_pkey_new PRIMARY KEY (station_id, measured_from, measured_to)
);

CREATE TABLE airqualitystations_measurements (
	station_id varchar(255) NOT NULL,
	measured_from timestamptz NOT NULL,
	measured_to timestamptz NOT NULL,
	component_code varchar(50) NOT NULL,
	aggregation_interval varchar(50) NOT NULL,
	value float8 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT airqualitystations_measurements_pkey_new PRIMARY KEY (station_id, measured_from, measured_to, component_code, aggregation_interval)
) PARTITION BY RANGE (measured_from);

CREATE INDEX airqualitystations_measurements_aggregation_interval_ix ON airqualitystations_measurements USING btree (aggregation_interval);
CREATE INDEX airqualitystations_measurements_component_code_ix ON airqualitystations_measurements USING btree (component_code);

-- partitions

CREATE TABLE airqualitystations_measurements_min PARTITION OF airqualitystations_measurements 
	FOR VALUES FROM (MINVALUE) TO ('2020-05-01');
CREATE TABLE airqualitystations_measurements_y2020m05 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2020-05-01') TO ('2020-06-01');
CREATE TABLE airqualitystations_measurements_y2020m06 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2020-06-01') TO ('2020-07-01');	
CREATE TABLE airqualitystations_measurements_y2020m07 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2020-07-01') TO ('2020-08-01');		
CREATE TABLE airqualitystations_measurements_y2020m08 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2020-08-01') TO ('2020-09-01');			
CREATE TABLE airqualitystations_measurements_y2020m09 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2020-09-01') TO ('2020-10-01');				
CREATE TABLE airqualitystations_measurements_y2020m10 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2020-10-01') TO ('2020-11-01');
CREATE TABLE airqualitystations_measurements_y2020m11 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2020-11-01') TO ('2020-12-01');	
CREATE TABLE airqualitystations_measurements_y2020m12 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2020-12-01') TO ('2021-01-01');		
	
-- 2021	

CREATE TABLE airqualitystations_measurements_y2021m01 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2021-01-01') TO ('2021-02-01');
CREATE TABLE airqualitystations_measurements_y2021m02 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2021-02-01') TO ('2021-03-01');
CREATE TABLE airqualitystations_measurements_y2021m03 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2021-03-01') TO ('2021-04-01');
CREATE TABLE airqualitystations_measurements_y2021m04 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2021-04-01') TO ('2021-05-01');
CREATE TABLE airqualitystations_measurements_y2021m05 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2021-05-01') TO ('2021-06-01');
CREATE TABLE airqualitystations_measurements_y2021m06 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2021-06-01') TO ('2021-07-01');	
CREATE TABLE airqualitystations_measurements_y2021m07 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2021-07-01') TO ('2021-08-01');		
CREATE TABLE airqualitystations_measurements_y2021m08 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2021-08-01') TO ('2021-09-01');			
CREATE TABLE airqualitystations_measurements_y2021m09 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2021-09-01') TO ('2021-10-01');				
CREATE TABLE airqualitystations_measurements_y2021m10 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2021-10-01') TO ('2021-11-01');
CREATE TABLE airqualitystations_measurements_y2021m11 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2021-11-01') TO ('2021-12-01');	
CREATE TABLE airqualitystations_measurements_y2021m12 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2021-12-01') TO ('2022-01-01');		

-- 2022	

CREATE TABLE airqualitystations_measurements_y2022m01 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2022-01-01') TO ('2022-02-01');
CREATE TABLE airqualitystations_measurements_y2022m02 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2022-02-01') TO ('2022-03-01');
CREATE TABLE airqualitystations_measurements_y2022m03 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2022-03-01') TO ('2022-04-01');
CREATE TABLE airqualitystations_measurements_y2022m04 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2022-04-01') TO ('2022-05-01');
CREATE TABLE airqualitystations_measurements_y2022m05 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2022-05-01') TO ('2022-06-01');
CREATE TABLE airqualitystations_measurements_y2022m06 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2022-06-01') TO ('2022-07-01');	
CREATE TABLE airqualitystations_measurements_y2022m07 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2022-07-01') TO ('2022-08-01');		
CREATE TABLE airqualitystations_measurements_y2022m08 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2022-08-01') TO ('2022-09-01');			
CREATE TABLE airqualitystations_measurements_y2022m09 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2022-09-01') TO ('2022-10-01');				
CREATE TABLE airqualitystations_measurements_y2022m10 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2022-10-01') TO ('2022-11-01');
CREATE TABLE airqualitystations_measurements_y2022m11 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2022-11-01') TO ('2022-12-01');	
CREATE TABLE airqualitystations_measurements_y2022m12 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2022-12-01') TO ('2023-01-01');		

-- 2023	

CREATE TABLE airqualitystations_measurements_y2023m01 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2023-01-01') TO ('2023-02-01');
CREATE TABLE airqualitystations_measurements_y2023m02 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2023-02-01') TO ('2023-03-01');
CREATE TABLE airqualitystations_measurements_y2023m03 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2023-03-01') TO ('2023-04-01');
CREATE TABLE airqualitystations_measurements_y2023m04 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2023-04-01') TO ('2023-05-01');
CREATE TABLE airqualitystations_measurements_y2023m05 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2023-05-01') TO ('2023-06-01');
CREATE TABLE airqualitystations_measurements_y2023m06 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2023-06-01') TO ('2023-07-01');	
CREATE TABLE airqualitystations_measurements_y2023m07 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2023-07-01') TO ('2023-08-01');		
CREATE TABLE airqualitystations_measurements_y2023m08 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2023-08-01') TO ('2023-09-01');			
CREATE TABLE airqualitystations_measurements_y2023m09 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2023-09-01') TO ('2023-10-01');				
CREATE TABLE airqualitystations_measurements_y2023m10 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2023-10-01') TO ('2023-11-01');
CREATE TABLE airqualitystations_measurements_y2023m11 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2023-11-01') TO ('2023-12-01');	
CREATE TABLE airqualitystations_measurements_y2023m12 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2023-12-01') TO ('2024-01-01');		

-- 2024	

CREATE TABLE airqualitystations_measurements_y2024m01 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2024-01-01') TO ('2024-02-01');
CREATE TABLE airqualitystations_measurements_y2024m02 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2024-02-01') TO ('2024-03-01');
CREATE TABLE airqualitystations_measurements_y2024m03 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2024-03-01') TO ('2024-04-01');
CREATE TABLE airqualitystations_measurements_y2024m04 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2024-04-01') TO ('2024-05-01');
CREATE TABLE airqualitystations_measurements_y2024m05 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2024-05-01') TO ('2024-06-01');
CREATE TABLE airqualitystations_measurements_y2024m06 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2024-06-01') TO ('2024-07-01');	
CREATE TABLE airqualitystations_measurements_y2024m07 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2024-07-01') TO ('2024-08-01');		
CREATE TABLE airqualitystations_measurements_y2024m08 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2024-08-01') TO ('2024-09-01');			
CREATE TABLE airqualitystations_measurements_y2024m09 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2024-09-01') TO ('2024-10-01');				
CREATE TABLE airqualitystations_measurements_y2024m10 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2024-10-01') TO ('2024-11-01');
CREATE TABLE airqualitystations_measurements_y2024m11 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2024-11-01') TO ('2024-12-01');	
CREATE TABLE airqualitystations_measurements_y2024m12 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2024-12-01') TO ('2025-01-01');		

-- 2025

CREATE TABLE airqualitystations_measurements_y2025m01 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2025-01-01') TO ('2025-02-01');
CREATE TABLE airqualitystations_measurements_y2025m02 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2025-02-01') TO ('2025-03-01');
CREATE TABLE airqualitystations_measurements_y2025m03 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2025-03-01') TO ('2025-04-01');
CREATE TABLE airqualitystations_measurements_y2025m04 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2025-04-01') TO ('2025-05-01');
CREATE TABLE airqualitystations_measurements_y2025m05 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2025-05-01') TO ('2025-06-01');
CREATE TABLE airqualitystations_measurements_y2025m06 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2025-06-01') TO ('2025-07-01');	
CREATE TABLE airqualitystations_measurements_y2025m07 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2025-07-01') TO ('2025-08-01');		
CREATE TABLE airqualitystations_measurements_y2025m08 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2025-08-01') TO ('2025-09-01');			
CREATE TABLE airqualitystations_measurements_y2025m09 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2025-09-01') TO ('2025-10-01');				
CREATE TABLE airqualitystations_measurements_y2025m10 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2025-10-01') TO ('2025-11-01');
CREATE TABLE airqualitystations_measurements_y2025m11 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2025-11-01') TO ('2025-12-01');	
CREATE TABLE airqualitystations_measurements_y2025m12 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2025-12-01') TO ('2026-01-01');		

-- >25
CREATE TABLE airqualitystations_measurements_max PARTITION OF airqualitystations_measurements
	FOR VALUES FROM ('2026-12-01') TO (maxvalue);		
