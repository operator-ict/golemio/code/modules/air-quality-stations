CREATE TABLE airqualitystations_index_types (
    "id" integer NOT NULL,
    "index_code" varchar(50) NOT NULL,
    "limit_gte" double precision,
    "limit_lt" double precision,
    "color" varchar(50) NOT NULL,
    "color_text" varchar(50) NOT NULL,
    "description_cs" text NOT NULL,
    "description_en" text NOT NULL,

    CONSTRAINT "airqualitystations_index_types_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO airqualitystations_index_types ("id", "index_code", "limit_gte", "limit_lt", "color", "color_text", "description_cs", "description_en") VALUES
    (1, '1A', '0.00', '0.34', '009900', '000000', 'velmi dobrá až dobrá', 'very good to good'),
    (2, '1B', '0.34', '0.67', '00CC00', '000000', 'velmi dobrá až dobrá', 'very good to good'),
    (3, '2A', '0.67', '1.00', 'FFF200', '000000', 'přijatelná', 'acceptable'),
    (4, '2B', '1.00', '1.50', 'FAA61A', 'FFFFFF', 'přijatelná', 'acceptable'),
    (5, '3A', '1.50', '2.00', 'ED1C24', 'FFFFFF', 'zhoršená až špatná', 'aggravated to bad'),
    (6, '3B', '2.00', NULL, '671F20', '000000', 'zhoršená až špatná', 'aggravated to bad'),
    (7, '0', NULL, NULL, 'FFFFFF', '000000', 'neúplná data', 'incomplete data'),
    (8, '-1', NULL, NULL, 'CFCFCF', '000000', 'index nestanoven', 'index not determined');

CREATE TABLE airqualitystations_component_types (
    "id" integer NOT NULL,
    "component_code" varchar(50) NOT NULL,
    "unit" varchar(50) NOT NULL,
    "description_cs" text NOT NULL,
    "description_en" text NOT NULL,

    CONSTRAINT "airqualitystations_component_types_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO airqualitystations_component_types ("id", "component_code", "unit", "description_cs", "description_en") VALUES
    (1, 'SO2', 'µg/m³', 'oxid siřičitý', 'sulphur dioxide'),
    (2, 'NO2', 'µg/m³', 'oxid dusičitý', 'nitrogen dioxide'),
    (3, 'PM10', 'µg/m³', 'částice PM10', 'particles PM10'),
    (4, 'O3', 'µg/m³', 'ozón', 'ozone'),
    (5, 'O3_Model', 'µg/m³', 'ozón (model)', 'ozone (model)'),
    (6, 'PM2_5', 'µg/m³', 'částice PM2,5', 'fine particles PM2.5'),
    (7, 'CO', 'µg/m³', 'oxid uhelnatý', 'carbon monoxide');


CREATE TABLE airqualitystations_indexes (
    "station_id" varchar(255) NOT NULL, -- airqualitystations.id, e.g. CZ_A_AKALA
    "measured_from" bigint NOT NULL, -- States[].DateFromUTC, e.g. 159242949294
    "measured_to" bigint NOT NULL, -- States[].DateToUTC, e.g. 159244949294
    "index_code" varchar(50) NOT NULL, -- airqualitystations_metadata_indexes.index_code, e.g. 1A

    "create_batch_id" bigint,
    "created_at" timestamp with time zone,
    "created_by" character varying(150),
    "update_batch_id" bigint,
    "updated_at" timestamp with time zone,
    "updated_by" character varying(150),
    CONSTRAINT airqualitystations_indexes_pkey PRIMARY KEY (station_id, measured_from, measured_to)
);

CREATE TABLE airqualitystations (
    "id" varchar(255) NOT NULL, -- States[].Code + "_" + States[].Regions[].Code + "_" + States[].Regions[].Stations[].Code, e.g. CZ_A_AKALA
    "state_code" varchar(50) NOT NULL, -- States[].Code, e.g. CZ
    "state_name" varchar(255) NOT NULL, -- States[].Name, e.g. Česká republika
    "region_code" varchar(50) NOT NULL, -- States[].Regions[].Code, e.g. A
    "region_name" varchar(255) NOT NULL, -- States[].Regions[].Name, e.g. Praha
    "station_vendor_id" varchar(255) NOT NULL, -- States[].Regions[].Stations[].Code, e.g. AKALA
    "station_name" varchar(255) NOT NULL, -- States[].Regions[].Stations[].Name, e.g. Praha 8-Karlín
    "owner" varchar(255), -- States[].Regions[].Stations[].Owner, e.g. ČHMÚ
    "classification" TEXT, -- States[].Regions[].Stations[].Classif, e.g. dopravní
    "latitude" double precision, -- States[].Regions[].Stations[].Lat, e.g. 50.094238
    "longitude" double precision, -- States[].Regions[].Stations[].Lon, e.g. 14.442049
    "district" varchar NULL,

    "create_batch_id" bigint,
    "created_at" timestamp with time zone,
    "created_by" character varying(150),
    "update_batch_id" bigint,
    "updated_at" timestamp with time zone,
    "updated_by" character varying(150),
	CONSTRAINT airqualitystations_pkey PRIMARY KEY (id)
);

CREATE TABLE airqualitystations_measurements (
	station_id varchar(255) NOT NULL,
	measured_from int8 NOT NULL,
	measured_to int8 NOT NULL,
	component_code varchar(50) NOT NULL,
	aggregation_interval varchar(50) NOT NULL,
	value float8 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT airqualitystations_measurements_pkey PRIMARY KEY (station_id, measured_from, measured_to, component_code, aggregation_interval)
) PARTITION BY RANGE (measured_from);

CREATE INDEX airqualitystations_measurements_aggregation_interval ON airqualitystations_measurements USING btree (aggregation_interval);
CREATE INDEX airqualitystations_measurements_component_code ON airqualitystations_measurements USING btree (component_code);

-- partitions

CREATE TABLE airqualitystations_measurements_min PARTITION OF airqualitystations_measurements 
	FOR VALUES FROM (MINVALUE) TO (extract ('epoch' from '2020-05-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2020m05 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2020-05-01'::timestamp)*1000) TO (extract ('epoch' from '2020-06-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2020m06 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2020-06-01'::timestamp)*1000) TO (extract ('epoch' from '2020-07-01'::timestamp)*1000);	
CREATE TABLE airqualitystations_measurements_y2020m07 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2020-07-01'::timestamp)*1000) TO (extract ('epoch' from '2020-08-01'::timestamp)*1000);		
CREATE TABLE airqualitystations_measurements_y2020m08 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2020-08-01'::timestamp)*1000) TO (extract ('epoch' from '2020-09-01'::timestamp)*1000);			
CREATE TABLE airqualitystations_measurements_y2020m09 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2020-09-01'::timestamp)*1000) TO (extract ('epoch' from '2020-10-01'::timestamp)*1000);				
CREATE TABLE airqualitystations_measurements_y2020m10 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2020-10-01'::timestamp)*1000) TO (extract ('epoch' from '2020-11-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2020m11 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2020-11-01'::timestamp)*1000) TO (extract ('epoch' from '2020-12-01'::timestamp)*1000);	
CREATE TABLE airqualitystations_measurements_y2020m12 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2020-12-01'::timestamp)*1000) TO (extract ('epoch' from '2021-01-01'::timestamp)*1000);		
	
-- 2021	

CREATE TABLE airqualitystations_measurements_y2021m01 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2021-01-01'::timestamp)*1000) TO (extract ('epoch' from '2021-02-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2021m02 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2021-02-01'::timestamp)*1000) TO (extract ('epoch' from '2021-03-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2021m03 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2021-03-01'::timestamp)*1000) TO (extract ('epoch' from '2021-04-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2021m04 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2021-04-01'::timestamp)*1000) TO (extract ('epoch' from '2021-05-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2021m05 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2021-05-01'::timestamp)*1000) TO (extract ('epoch' from '2021-06-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2021m06 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2021-06-01'::timestamp)*1000) TO (extract ('epoch' from '2021-07-01'::timestamp)*1000);	
CREATE TABLE airqualitystations_measurements_y2021m07 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2021-07-01'::timestamp)*1000) TO (extract ('epoch' from '2021-08-01'::timestamp)*1000);		
CREATE TABLE airqualitystations_measurements_y2021m08 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2021-08-01'::timestamp)*1000) TO (extract ('epoch' from '2021-09-01'::timestamp)*1000);			
CREATE TABLE airqualitystations_measurements_y2021m09 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2021-09-01'::timestamp)*1000) TO (extract ('epoch' from '2021-10-01'::timestamp)*1000);				
CREATE TABLE airqualitystations_measurements_y2021m10 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2021-10-01'::timestamp)*1000) TO (extract ('epoch' from '2021-11-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2021m11 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2021-11-01'::timestamp)*1000) TO (extract ('epoch' from '2021-12-01'::timestamp)*1000);	
CREATE TABLE airqualitystations_measurements_y2021m12 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2021-12-01'::timestamp)*1000) TO (extract ('epoch' from '2022-01-01'::timestamp)*1000);		

-- 2022	

CREATE TABLE airqualitystations_measurements_y2022m01 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2022-01-01'::timestamp)*1000) TO (extract ('epoch' from '2022-02-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2022m02 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2022-02-01'::timestamp)*1000) TO (extract ('epoch' from '2022-03-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2022m03 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2022-03-01'::timestamp)*1000) TO (extract ('epoch' from '2022-04-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2022m04 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2022-04-01'::timestamp)*1000) TO (extract ('epoch' from '2022-05-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2022m05 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2022-05-01'::timestamp)*1000) TO (extract ('epoch' from '2022-06-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2022m06 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2022-06-01'::timestamp)*1000) TO (extract ('epoch' from '2022-07-01'::timestamp)*1000);	
CREATE TABLE airqualitystations_measurements_y2022m07 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2022-07-01'::timestamp)*1000) TO (extract ('epoch' from '2022-08-01'::timestamp)*1000);		
CREATE TABLE airqualitystations_measurements_y2022m08 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2022-08-01'::timestamp)*1000) TO (extract ('epoch' from '2022-09-01'::timestamp)*1000);			
CREATE TABLE airqualitystations_measurements_y2022m09 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2022-09-01'::timestamp)*1000) TO (extract ('epoch' from '2022-10-01'::timestamp)*1000);				
CREATE TABLE airqualitystations_measurements_y2022m10 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2022-10-01'::timestamp)*1000) TO (extract ('epoch' from '2022-11-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2022m11 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2022-11-01'::timestamp)*1000) TO (extract ('epoch' from '2022-12-01'::timestamp)*1000);	
CREATE TABLE airqualitystations_measurements_y2022m12 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2022-12-01'::timestamp)*1000) TO (extract ('epoch' from '2023-01-01'::timestamp)*1000);		

-- 2023	

CREATE TABLE airqualitystations_measurements_y2023m01 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2023-01-01'::timestamp)*1000) TO (extract ('epoch' from '2023-02-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2023m02 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2023-02-01'::timestamp)*1000) TO (extract ('epoch' from '2023-03-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2023m03 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2023-03-01'::timestamp)*1000) TO (extract ('epoch' from '2023-04-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2023m04 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2023-04-01'::timestamp)*1000) TO (extract ('epoch' from '2023-05-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2023m05 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2023-05-01'::timestamp)*1000) TO (extract ('epoch' from '2023-06-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2023m06 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2023-06-01'::timestamp)*1000) TO (extract ('epoch' from '2023-07-01'::timestamp)*1000);	
CREATE TABLE airqualitystations_measurements_y2023m07 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2023-07-01'::timestamp)*1000) TO (extract ('epoch' from '2023-08-01'::timestamp)*1000);		
CREATE TABLE airqualitystations_measurements_y2023m08 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2023-08-01'::timestamp)*1000) TO (extract ('epoch' from '2023-09-01'::timestamp)*1000);			
CREATE TABLE airqualitystations_measurements_y2023m09 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2023-09-01'::timestamp)*1000) TO (extract ('epoch' from '2023-10-01'::timestamp)*1000);				
CREATE TABLE airqualitystations_measurements_y2023m10 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2023-10-01'::timestamp)*1000) TO (extract ('epoch' from '2023-11-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2023m11 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2023-11-01'::timestamp)*1000) TO (extract ('epoch' from '2023-12-01'::timestamp)*1000);	
CREATE TABLE airqualitystations_measurements_y2023m12 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2023-12-01'::timestamp)*1000) TO (extract ('epoch' from '2024-01-01'::timestamp)*1000);		

-- 2024	

CREATE TABLE airqualitystations_measurements_y2024m01 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2024-01-01'::timestamp)*1000) TO (extract ('epoch' from '2024-02-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2024m02 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2024-02-01'::timestamp)*1000) TO (extract ('epoch' from '2024-03-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2024m03 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2024-03-01'::timestamp)*1000) TO (extract ('epoch' from '2024-04-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2024m04 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2024-04-01'::timestamp)*1000) TO (extract ('epoch' from '2024-05-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2024m05 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2024-05-01'::timestamp)*1000) TO (extract ('epoch' from '2024-06-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2024m06 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2024-06-01'::timestamp)*1000) TO (extract ('epoch' from '2024-07-01'::timestamp)*1000);	
CREATE TABLE airqualitystations_measurements_y2024m07 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2024-07-01'::timestamp)*1000) TO (extract ('epoch' from '2024-08-01'::timestamp)*1000);		
CREATE TABLE airqualitystations_measurements_y2024m08 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2024-08-01'::timestamp)*1000) TO (extract ('epoch' from '2024-09-01'::timestamp)*1000);			
CREATE TABLE airqualitystations_measurements_y2024m09 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2024-09-01'::timestamp)*1000) TO (extract ('epoch' from '2024-10-01'::timestamp)*1000);				
CREATE TABLE airqualitystations_measurements_y2024m10 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2024-10-01'::timestamp)*1000) TO (extract ('epoch' from '2024-11-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2024m11 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2024-11-01'::timestamp)*1000) TO (extract ('epoch' from '2024-12-01'::timestamp)*1000);	
CREATE TABLE airqualitystations_measurements_y2024m12 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2024-12-01'::timestamp)*1000) TO (extract ('epoch' from '2025-01-01'::timestamp)*1000);		

-- 2025

CREATE TABLE airqualitystations_measurements_y2025m01 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2025-01-01'::timestamp)*1000) TO (extract ('epoch' from '2025-02-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2025m02 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2025-02-01'::timestamp)*1000) TO (extract ('epoch' from '2025-03-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2025m03 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2025-03-01'::timestamp)*1000) TO (extract ('epoch' from '2025-04-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2025m04 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2025-04-01'::timestamp)*1000) TO (extract ('epoch' from '2025-05-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2025m05 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2025-05-01'::timestamp)*1000) TO (extract ('epoch' from '2025-06-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2025m06 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2025-06-01'::timestamp)*1000) TO (extract ('epoch' from '2025-07-01'::timestamp)*1000);	
CREATE TABLE airqualitystations_measurements_y2025m07 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2025-07-01'::timestamp)*1000) TO (extract ('epoch' from '2025-08-01'::timestamp)*1000);		
CREATE TABLE airqualitystations_measurements_y2025m08 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2025-08-01'::timestamp)*1000) TO (extract ('epoch' from '2025-09-01'::timestamp)*1000);			
CREATE TABLE airqualitystations_measurements_y2025m09 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2025-09-01'::timestamp)*1000) TO (extract ('epoch' from '2025-10-01'::timestamp)*1000);				
CREATE TABLE airqualitystations_measurements_y2025m10 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2025-10-01'::timestamp)*1000) TO (extract ('epoch' from '2025-11-01'::timestamp)*1000);
CREATE TABLE airqualitystations_measurements_y2025m11 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2025-11-01'::timestamp)*1000) TO (extract ('epoch' from '2025-12-01'::timestamp)*1000);	
CREATE TABLE airqualitystations_measurements_y2025m12 PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2025-12-01'::timestamp)*1000) TO (extract ('epoch' from '2026-01-01'::timestamp)*1000);		

-- >25
CREATE TABLE airqualitystations_measurements_max PARTITION OF airqualitystations_measurements
	FOR VALUES FROM (extract ('epoch' from '2026-12-01'::timestamp)*1000) TO (maxvalue);		