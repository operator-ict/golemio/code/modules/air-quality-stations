# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.3.3] - 2024-11-28

### Added

-   AsyncAPI documentation ([integration-engine#263](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/263))

## [1.3.2] - 2024-09-12

### Changed

-   `.gitlab-ci.yml` cleanup ([devops#320](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/issues/320))

## [1.3.1] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.3.0] - 2024-08-13

### Changed

-   Refactoring and update of module ([air-quality-stations#9](https://gitlab.com/operator-ict/golemio/code/modules/air-quality-stations/-/issues/9))

## [1.2.0] - 2024-06-03

### Added

-   The `Cache-Control` header to all output gateway responses ([core#105](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/105))
-   Missing router tests (and the supertest dev dependency)

### Changed

-   Updated OpenAPI docs

## [1.1.12] - 2024-05-22

### Fixed

-   Filter out invalid GeoJSON items ([general#572](https://gitlab.com/operator-ict/golemio/code/general/-/issues/572))

## [1.1.11] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.1.10] - 2024-04-08

### Changed

-   axios exchanged for native fetch ([core#99](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/99))

## [1.1.9] - 2024-03-25

### Fixed

-   Fixed router query validation rules ([core#93](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/93))

## [1.1.8] - 2023-09-27

### Changed

-   API versioning - version moved to path ([core#80](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/80))

### Fixed

-   Load env vars for testing

## [1.1.7] - 2023-09-13

### Changed

-   Transform times from bigint to timestamp with time zone ([air-quality-stations#8](https://gitlab.com/operator-ict/golemio/code/modules/air-quality-stations/-/issues/8))

## [1.1.6] - 2023-08-23

### Changed

-   Run integration apidocs tests via Golemio CLI ([core#46](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/46))

## [1.1.5] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [1.1.4] - 2023-05-29

### Changed

-   Update openapi docs

## [1.1.3] - 2023-03-08

### Changed

-   Convert Mongoose schema definitions to JSON schemas ([general#435](https://gitlab.com/operator-ict/golemio/code/general/-/issues/435))

## [1.1.2] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.1.1] - 2023-02-22

### Changed

-   Update city-districts

## [1.1.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.0.7] - 2022-11-18

-   Update gitlab-ci template

## [1.0.6] - 2022-07-18

### Added

-   !apidoc to npm.ignore [OG#79](https://gitlab.com/operator-ict/golemio/code/input-gateway/-/issues/79)

## [1.0.5] - 2022-06-21

### Changed

-   DB Schema separation ([air-quality-stations#6](https://gitlab.com/operator-ict/golemio/code/modules/air-quality-stations/-/issues/6))
-   Typescript version update from 4.4.4 to 4.6.4

## [1.0.4] - 2022-05-02

### Fixed

-   Fix for output of history endpoint. Updated_at showed when station was updated but in history context we want to see when the air quality index was calculated.

## [1.0.3] - 2022-04-07

### Changed

-   Postgres model refactored into separate file.

### Added

-   New column district for Air Quality Stations, new queue and worker method that fills that column.

