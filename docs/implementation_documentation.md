# Implementační dokumentace modulu *air-quality-stations*

Modul zpracovává měření kvality vzduchu z dat ČHMÚ.

## Zpracování dat / transformace

Interní RabbitMQ fronty jsou popsány v [AsyncAPI](./asyncapi.yaml).
