import { AirQualityIndexesRepository } from "#og/repositories/AirQualityIndexesRepository";
import { expect } from "chai";

describe("AirQualityIndexesRepository", () => {
    const model = new AirQualityIndexesRepository();

    it("formats undefined correctly", () => {
        const actual = model.formatResult(undefined);
        const expected = {
            features: [],
            type: "FeatureCollection",
        };
        expect(actual).to.deep.eq(expected);
    });

    it("result without geometry is empty", () => {
        const result = [
            {
                index_code: "asd",
                measured_from: "2024-01-02",
                measured_to: "2024-01-03",
                station_id: "asdf",
                updated_at: "2024-01-03 14:30",
            },
        ];

        const actual = model.formatResult(result);
        const expected = {
            features: [],
            type: "FeatureCollection",
        };
        expect(actual).to.deep.eq(expected);
    });
});
