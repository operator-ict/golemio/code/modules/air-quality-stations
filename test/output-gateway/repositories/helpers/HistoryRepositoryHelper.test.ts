import HistoryRepositoryHelper from "#og/repositories/helpers/HistoryRepositoryHelper";
import { expect } from "chai";

describe("HistoryModelHelper", () => {
    const someUnixTime = "2019-05-16T04:27:58.000Z";

    it("filterByMeasuredFrom - should return filter", () => {
        const filters = HistoryRepositoryHelper.filterByMeasuredFrom({
            from: someUnixTime,
        });
        expect(filters.length).to.be.equal(1);
    });

    it("filterByMeasuredTo - should return filter", () => {
        const filters = HistoryRepositoryHelper.filterByMeasuredTo({
            to: someUnixTime,
        });
        expect(filters.length).to.be.equal(1);
    });

    it("filterByStationId - should return filter", () => {
        const filters = HistoryRepositoryHelper.filterByStationId({
            sensorId: "ABCD",
        });
        expect(filters.length).to.be.equal(1);
    });

    it("filterByMeasuredFrom - no filter", () => {
        const filters = HistoryRepositoryHelper.filterByMeasuredFrom({
            from: undefined,
        });
        expect(filters.length).to.be.equal(0);
    });

    it("filterByStationId - no filter", () => {
        const filters = HistoryRepositoryHelper.filterByStationId({});
        expect(filters.length).to.be.equal(0);
    });
});
