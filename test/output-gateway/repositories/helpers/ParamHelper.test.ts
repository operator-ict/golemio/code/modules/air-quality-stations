import { ParamHelper } from "#og/repositories/helpers/ParamHelper";
import { expect } from "chai";

describe("ParamHelper", () => {
    it("should return number limit for query", () => {
        expect(ParamHelper.getPaginationLimit(10001)).to.be.equal(10000);
        expect(ParamHelper.getPaginationLimit(-1)).to.be.equal(10000);
        expect(ParamHelper.getPaginationLimit(undefined)).to.be.equal(10000);
        expect(ParamHelper.getPaginationLimit(NaN)).to.be.equal(10000);
        expect(ParamHelper.getPaginationLimit(52)).to.be.equal(52);
        expect(ParamHelper.getPaginationLimit(0)).to.be.equal(0);
    });
});
