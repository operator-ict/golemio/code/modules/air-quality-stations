import { buildIndexesQuery } from "#og/repositories/helpers/RawQueryProvider";
import { expect } from "chai";

describe("RawQueryProvider", () => {
    it("should return query with all variable parts", () => {
        const query = buildIndexesQuery({
            offset: 4,
            limit: 7,
            lat: 50.066429,
            lng: 14.446152,
            range: 1000,
            updatedSince: "2022-03-28T11:10:26.115Z",
            districts: ["praha-7"],
        });
        expect(query).to.contain("limit :limit");
        expect(query).to.contain("offset :offset");
        expect(query).to.contain("distance");
        expect(query).to.contain("ST_DWithin");
        expect(query).to.contain(":updatedAt");
        expect(query).to.contain(":districts");
        expect(query).to.contain("order by");
    });

    it("should return query with none variable parts", () => {
        const query = buildIndexesQuery({});
        expect(query).to.not.contain("limit :limit");
        expect(query).to.not.contain("offset :offset");
        expect(query).to.not.contain("distance");
        expect(query).to.not.contain("ST_DWithin");
        expect(query).to.not.contain(":updatedAt");
        expect(query).to.not.contain(":districts");
        expect(query).to.not.contain("order by");
    });

    it("should return query with some variable parts", () => {
        const query = buildIndexesQuery({
            offset: 0,
            lat: 50.066429,
            lng: 14.446152,
            districts: ["praha-7", "praha-5"],
        });
        expect(query).to.not.contain("limit :limit");
        expect(query).to.contain("offset :offset");
        expect(query).to.contain("distance");
        expect(query).to.not.contain("ST_DWithin"); // only when range is provided
        expect(query).to.not.contain(":updatedAt");
        expect(query).to.contain(":districts");
        expect(query).to.contain("order by");
    });
});
