import OutputDtoMapper from "#og/repositories/helpers/OutputDtoMapper";
import IAirQualityIndex from "#sch/models/interfaces/IAirQualityIndex";
import IAirQualityMeasurement from "#sch/models/interfaces/IAirQualityMeasurement";
import IAirQualityStation from "#sch/models/interfaces/IAirQualityStation";
import { expect } from "chai";

describe("OutputDtoMapper", () => {
    const mockup: IAirQualityIndex = {
        index_code: "2A",
        measured_from: "2019-05-16T04:27:58.000Z",
        measured_to: "2019-05-18T04:27:58.000Z",
        station_id: "CZ_A_AVRSA",
        updated_at: "2022-03-28T12:10:26.115Z",
        station: {
            classification: "T/U/CR",
            id: "CZ_A_AVYNA",
            latitude: 50.11108,
            longitude: 14.503096,
            station_vendor_id: "AVYNA",
            district: "praha-9",
            updated_at: "2022-03-28T11:10:26.115Z",
        } as IAirQualityStation,
        measurements: [
            {
                aggregation_interval: "3h",
                component_code: "PM10",
                station_id: "CZ_A_AVRSA",
                value: 18.7,
            } as IAirQualityMeasurement,
            {
                aggregation_interval: "3h",
                component_code: "O3_Model",
                station_id: "CZ_A_AVRSA",
                value: null,
            } as IAirQualityMeasurement,
            {
                aggregation_interval: "3h",
                component_code: "SO2",
                station_id: "CZ_A_AVRSA",
                value: 3.6,
            } as IAirQualityMeasurement,
        ],
    };

    it("should map mockup to feature output dto", () => {
        const result = OutputDtoMapper.mapToFeatureItem(mockup);
        const properties: any = result?.properties;
        expect(result?.type).to.equal("Feature");
        expect(properties.id).to.equal("AVYNA");
        expect(properties.updated_at).to.be.equal("2022-03-28T11:10:26.115Z");
        expect(properties.measurement.components.length).to.equal(2);
    });

    it("should map mockup to historic output dto", () => {
        const result = OutputDtoMapper.mapToHistoricItem(mockup);
        expect(result?.type).to.be.undefined;
        expect(result?.properties).to.be.undefined;
        expect(result.updated_at).to.be.equal("2022-03-28T12:10:26.115Z");
        expect(result.measurement.AQ_hourly_index).to.equal("2A");
        expect(result.measurement.components.length).to.equal(2);
    });
});
