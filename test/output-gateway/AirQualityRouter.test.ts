import { airQualityRouter } from "#og";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import request from "supertest";

chai.use(chaiAsPromised);

describe("AirQualityRouter", () => {
    const app = express();

    before(() => {
        app.use("/airqualitystations", airQualityRouter);
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    describe("GET /airqualitystations", () => {
        it("should respond with JSON", (done) => {
            request(app)
                .get("/airqualitystations")
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .then((response) => {
                    expect(response.body?.type).to.equal("FeatureCollection");
                    expect(response.body?.features).to.be.an("array");
                    done();
                })
                .catch((err: unknown) => done(err));
        });

        it("should respond with the correct Cache-Control headers", (done) => {
            request(app)
                .get("/airqualitystations")
                .set("Accept", "application/json")
                .expect(200)
                .then((response) => {
                    expect(response.headers["cache-control"]).to.equal("public, s-maxage=1200, stale-while-revalidate=300");
                    done();
                })
                .catch((err: unknown) => done(err));
        });
    });

    describe("GET /airqualitystations/componenttypes", () => {
        it("should respond with JSON", (done) => {
            request(app)
                .get("/airqualitystations/componenttypes")
                .expect(200)
                .then((response) => {
                    expect(response.body).to.be.an("array");
                    done();
                })
                .catch((err: unknown) => done(err));
        });

        it("should respond with the correct Cache-Control headers", (done) => {
            request(app)
                .get("/airqualitystations/componenttypes")
                .set("Accept", "application/json")
                .expect(200)
                .then((response) => {
                    expect(response.headers["cache-control"]).to.equal("public, s-maxage=1200, stale-while-revalidate=300");
                    done();
                })
                .catch((err: unknown) => done(err));
        });
    });

    describe("GET /airqualitystations/history", () => {
        it("should respond with JSON", (done) => {
            request(app)
                .get(
                    "/airqualitystations/history?from=2019-05-16T04%3A27%3A58.000Z&to=2019-05-18T04%3A27%3A58.000Z&sensorId=12345"
                )
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .then((response) => {
                    expect(response.body).to.be.an("array");
                    done();
                })
                .catch((err: unknown) => done(err));
        });

        it("should respond with the correct Cache-Control headers", (done) => {
            request(app)
                .get(
                    "/airqualitystations/history?from=2019-05-16T04%3A27%3A58.000Z&to=2019-05-18T04%3A27%3A58.000Z&sensorId=12345"
                )
                .set("Accept", "application/json")
                .expect(200)
                .then((response) => {
                    expect(response.headers["cache-control"]).to.equal("public, s-maxage=1200, stale-while-revalidate=300");
                    done();
                })
                .catch((err: unknown) => done(err));
        });
    });

    describe("GET /airqualitystations/indextypes", () => {
        it("should respond with JSON", (done) => {
            request(app)
                .get("/airqualitystations/indextypes")
                .expect(200)
                .then((response) => {
                    expect(response.body).to.be.an("array");
                    done();
                })
                .catch((err: unknown) => done(err));
        });

        it("should respond with the correct Cache-Control headers", (done) => {
            request(app)
                .get("/airqualitystations/indextypes")
                .set("Accept", "application/json")
                .expect(200)
                .then((response) => {
                    expect(response.headers["cache-control"]).to.equal("public, s-maxage=1200, stale-while-revalidate=300");
                    done();
                })
                .catch((err: unknown) => done(err));
        });
    });
});
