import { IEModuleContainer } from "#ie/ioc/Di";
import { IEModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { Refresh3HDataInDBTask } from "#ie/workers/tasks/Refresh3HDataInDBTask";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { QueueManager } from "@golemio/core/dist/integration-engine";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import { createSandbox, SinonSandbox, SinonSpy, SinonStub } from "sinon";
import { dataSource3hData } from "../../data/datasource-3h-data";
import { dataSource3hDataTransformed } from "../../data/datasource-3h-data-transformed";

describe("Refresh3HDataInDBTask", () => {
    let sandbox: SinonSandbox;
    let testContainer: DependencyContainer;
    let task: Refresh3HDataInDBTask;
    let getDatasourceStub: SinonStub;
    let transformationStub: SinonStub;
    let sequelizeModelStub: SinonStub;

    beforeEach(async () => {
        sandbox = createSandbox();

        getDatasourceStub = sandbox.stub().returns({ getAll: sandbox.stub().resolves(dataSource3hData) });
        transformationStub = sandbox.stub().returns(dataSource3hDataTransformed);

        const postgresConnector = IEModuleContainer.resolve<IPostgresConnector>(CoreToken.PostgresConnector);
        sandbox.stub(postgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub(),
                transaction: sandbox.stub().callsFake(() => ({ commit: sandbox.stub() })),
            })
        );

        sequelizeModelStub = Object.assign({
            findAll: sandbox.stub().callsFake(() => {
                return [{ id: 1 }, { id: 2 }];
            }),
        });

        sandbox.stub(QueueManager, "sendMessageToExchange" as any);

        testContainer = IEModuleContainer.createChildContainer()
            .registerSingleton(
                IEModuleContainerToken.Datasource3HData,
                class DummyFactory {
                    getDataSource() {
                        return getDatasourceStub();
                    }
                }
            )
            .registerSingleton(
                IEModuleContainerToken.AirQualityStationsTransformation,
                class DummyTransformation {
                    transformElement = transformationStub;
                }
            )
            .registerSingleton(
                IEModuleContainerToken.StationsRepository,
                class DummyRepository {
                    bulkSave = sandbox.stub().resolves();
                    sequelizeModel = sequelizeModelStub;
                }
            )
            .registerSingleton(
                IEModuleContainerToken.MeasurementsRepository,
                class DummyRepository {
                    bulkSave = sandbox.stub().resolves();
                }
            )
            .registerSingleton(
                IEModuleContainerToken.IndexesRepository,
                class DummyRepository {
                    bulkSave = sandbox.stub().resolves();
                }
            );

        task = testContainer.resolve(IEModuleContainerToken.Refresh3HDataInDBTask);
    });

    afterEach(() => {
        testContainer.clearInstances();
        sandbox.restore();
    });

    it("should execute refresh3HDataInDB task", async () => {
        await task["execute"]();
        sandbox.assert.calledOnce(task["datasource"].getDataSource().getAll as SinonSpy);
        sandbox.assert.calledOnce(task["transformation"].transformElement as SinonSpy);
        sandbox.assert.calledWith(task["transformation"].transformElement as SinonSpy, dataSource3hData);
        sandbox.assert.calledOnce(task["chmiDataService"]["stationsRepository"].bulkSave as SinonSpy);
        sandbox.assert.calledOnce(task["chmiDataService"]["measurementsRepository"].bulkSave as SinonSpy);
        sandbox.assert.calledOnce(task["chmiDataService"]["indexesRepository"].bulkSave as SinonSpy);
        sandbox.assert.calledOnce(task["stationsRepository"]["sequelizeModel"].findAll as SinonSpy);
        sandbox.assert.callCount(QueueManager.sendMessageToExchange as SinonSpy, 2);
        sandbox.assert.callOrder(
            task["datasource"].getDataSource().getAll as SinonSpy,
            task["transformation"].transformElement as SinonSpy,
            task["chmiDataService"]["stationsRepository"].bulkSave as SinonSpy,
            task["chmiDataService"]["measurementsRepository"].bulkSave as SinonSpy,
            task["chmiDataService"]["indexesRepository"].bulkSave as SinonSpy,
            task["stationsRepository"]["sequelizeModel"].findAll as SinonSpy,
            QueueManager.sendMessageToExchange as SinonSpy
        );
    });
});
