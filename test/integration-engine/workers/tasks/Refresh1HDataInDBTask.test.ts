import { IEModuleContainer } from "#ie/ioc/Di";
import { IEModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { Refresh1HDataInDBTask } from "#ie/workers/tasks/Refresh1HDataInDBTask";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import { createSandbox, SinonSandbox, SinonSpy, SinonStub } from "sinon";
import { dataSource1hData } from "../../data/datasource-1h-data";
import { dataSource1hDataTransformed } from "../../data/datasource-1h-data-transformed";

describe("Refresh1HDataInDBTask", () => {
    let sandbox: SinonSandbox;
    let testContainer: DependencyContainer;
    let task: Refresh1HDataInDBTask;
    let getDatasourceStub: SinonStub;
    let transformationStub: SinonStub;

    beforeEach(async () => {
        sandbox = createSandbox();

        getDatasourceStub = sandbox.stub().returns({ getAll: sandbox.stub().resolves(dataSource1hData) });
        transformationStub = sandbox.stub().returns(dataSource1hDataTransformed);

        const postgresConnector = IEModuleContainer.resolve<IPostgresConnector>(CoreToken.PostgresConnector);
        sandbox.stub(postgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub(),
                transaction: sandbox.stub().callsFake(() => ({ commit: sandbox.stub() })),
            })
        );

        testContainer = IEModuleContainer.createChildContainer()
            .registerSingleton(
                IEModuleContainerToken.Datasource1HData,
                class DummyFactory {
                    getDataSource() {
                        return getDatasourceStub();
                    }
                }
            )
            .registerSingleton(
                IEModuleContainerToken.AirQualityStationsTransformation,
                class DummyTransformation {
                    transformElement = transformationStub;
                }
            )
            .registerSingleton(
                IEModuleContainerToken.StationsRepository,
                class DummyRepository {
                    bulkSave = sandbox.stub().resolves();
                }
            )
            .registerSingleton(
                IEModuleContainerToken.MeasurementsRepository,
                class DummyRepository {
                    bulkSave = sandbox.stub().resolves();
                }
            )
            .registerSingleton(
                IEModuleContainerToken.IndexesRepository,
                class DummyRepository {
                    bulkSave = sandbox.stub().resolves();
                }
            );

        task = testContainer.resolve(IEModuleContainerToken.Refresh1HDataInDBTask);
    });

    afterEach(() => {
        testContainer.clearInstances();
        sandbox.restore();
    });

    it("should execute refresh1HDataInDB task", async () => {
        await task["execute"]();
        sandbox.assert.calledOnce(task["datasource"].getDataSource().getAll as SinonSpy);
        sandbox.assert.calledOnce(task["transformation"].transformElement as SinonSpy);
        sandbox.assert.calledWith(task["transformation"].transformElement as SinonSpy, dataSource1hData);
        sandbox.assert.calledOnce(task["chmiDataService"]["stationsRepository"].bulkSave as SinonSpy);
        sandbox.assert.calledOnce(task["chmiDataService"]["measurementsRepository"].bulkSave as SinonSpy);
        sandbox.assert.calledOnce(task["chmiDataService"]["indexesRepository"].bulkSave as SinonSpy);
        sandbox.assert.callOrder(
            task["datasource"].getDataSource().getAll as SinonSpy,
            task["transformation"].transformElement as SinonSpy,
            task["chmiDataService"]["stationsRepository"].bulkSave as SinonSpy,
            task["chmiDataService"]["measurementsRepository"].bulkSave as SinonSpy,
            task["chmiDataService"]["indexesRepository"].bulkSave as SinonSpy
        );
    });
});
