import { IEModuleContainer } from "#ie/ioc/Di";
import { IEModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { UpdateDistrictTask } from "#ie/workers/tasks/UpdateDistrictTask";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { QueueManager } from "@golemio/core/dist/integration-engine";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import { createSandbox, SinonSandbox, SinonSpy, SinonStub } from "sinon";

describe("UpdateDistrictTask", () => {
    let sandbox: SinonSandbox;
    let testContainer: DependencyContainer;
    let task: UpdateDistrictTask;
    let updateStub: SinonStub;

    beforeEach(async () => {
        sandbox = createSandbox();
        const postgresConnector = IEModuleContainer.resolve<IPostgresConnector>(CoreToken.PostgresConnector);
        sandbox
            .stub(postgresConnector, "getConnection")
            .callsFake(() => Object.assign({ define: sandbox.stub(), transaction: sandbox.stub() }));

        updateStub = sandbox.stub();

        sandbox.stub(QueueManager, "sendMessageToExchange" as any);

        testContainer = IEModuleContainer.createChildContainer().registerSingleton(
            IEModuleContainerToken.StationsRepository,
            class DummyRepository {
                save = sandbox.stub().resolves();
                findOne = sandbox.stub().resolves({ district: null, save: updateStub });
            }
        );

        task = testContainer.resolve(IEModuleContainerToken.UpdateDistrictTask);

        sandbox.stub(task["cityDistrictsRepository"], "getDistrict").resolves();
    });

    afterEach(() => {
        testContainer.clearInstances();
        sandbox.restore();
    });

    it("should execute updateDistrict task", async () => {
        await task["execute"]({ id: "1" });
        sandbox.assert.calledOnce(task["stationsRepository"].findOne as SinonSpy);
        sandbox.assert.calledOnce(task["cityDistrictsRepository"].getDistrict as SinonSpy);
        sandbox.assert.calledOnce(updateStub as SinonSpy);
        sandbox.assert.callOrder(
            task["stationsRepository"].findOne as SinonSpy,
            task["cityDistrictsRepository"].getDistrict as SinonSpy,
            updateStub
        );
    });
});
