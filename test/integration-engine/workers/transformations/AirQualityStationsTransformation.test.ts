import { AirQualityIndexModel, AirQualityMeasurementModel, AirQualityStationModel } from "#sch/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { AirQualityStationsTransformation } from "#ie/workers/transformations/AirQualityStationsTransformation";
import { dataSource1hData } from "../../data/datasource-1h-data";
import { dataSource1hDataTransformed } from "../../data/datasource-1h-data-transformed";
import { dataSource3hData } from "../../data/datasource-3h-data";
import { dataSource3hDataTransformed } from "../../data/datasource-3h-data-transformed";

chai.use(chaiAsPromised);

describe("AirQualityStationsTransformation", () => {
    let transformation: AirQualityStationsTransformation;
    let stationsValidator: JSONSchemaValidator;
    let measurementsValidator: JSONSchemaValidator;
    let indexesValidator: JSONSchemaValidator;

    before(() => {
        stationsValidator = new JSONSchemaValidator("StationsRepositoryValidator", AirQualityStationModel.jsonSchema);
        measurementsValidator = new JSONSchemaValidator("MeasurementsRepositoryValidator", AirQualityMeasurementModel.jsonSchema);
        indexesValidator = new JSONSchemaValidator("IndexesRepositoryValidator", AirQualityIndexModel.jsonSchema);
        transformation = new AirQualityStationsTransformation();
    });

    it("should have name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("AirQualityStationsTransformation");
    });

    it("should properly transform 1h collection", async () => {
        const data = await transformation.transformElement(dataSource1hData);

        await expect(stationsValidator.Validate(data.stations)).to.be.fulfilled;
        await expect(measurementsValidator.Validate(data.measurements)).to.be.fulfilled;
        await expect(indexesValidator.Validate(data.indexes)).to.be.fulfilled;

        expect(data).be.eql(dataSource1hDataTransformed);
    });

    it("should properly transform 3h collection", async () => {
        const data = await transformation.transformElement(dataSource3hData);

        await expect(stationsValidator.Validate(data.stations)).to.be.fulfilled;
        await expect(measurementsValidator.Validate(data.measurements)).to.be.fulfilled;
        await expect(indexesValidator.Validate(data.indexes)).to.be.fulfilled;

        expect(data).be.eql(dataSource3hDataTransformed);
    });
});
