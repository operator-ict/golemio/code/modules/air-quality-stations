export const dataSource1hData = {
    Actualized: "15.05.2020 15:00 SELČ",
    Version: 1,
    Note: "Encoded in UTF-8. Contact vanek(@)idea-envi.cz if you need JAVA code to read this JSON",
    Components: [
        {
            Code: "SO2",
            Name: "oxid siřičitý",
            Unit: "µg/m³",
        },
        {
            Code: "NO2",
            Name: "oxid dusičitý",
            Unit: "µg/m³",
        },
        {
            Code: "CO",
            Name: "oxid uhelnatý",
            Unit: "µg/m³",
        },
        {
            Code: "O3",
            Name: "ozon",
            Unit: "µg/m³",
        },
        {
            Code: "PM10",
            Name: "částice PM10",
            Unit: "µg/m³",
        },
        {
            Code: "PM2_5",
            Name: "jemné částice PM2,5",
            Unit: "µg/m³",
        },
    ],
    States: [
        {
            Name: "Česká republika",
            Code: "CZ",
            DateFromTo: "15.05.2020 14:00 - 15:00 SELČ",
            DateFromUTC: "2020-05-15 12:00:00.0 UTC",
            DateToUTC: "2020-05-15 13:00:00.0 UTC",
            Regions: [
                {
                    Name: "Praha",
                    Code: "A",
                    Stations: [
                        {
                            Code: "ABREA",
                            Name: "Praha 6-Břevnov",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "50.084385",
                            Lon: "14.380116",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "23.5",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "11.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "18.1",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "ACHOA",
                            Name: "Praha 4-Chodov",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "50.030170",
                            Lon: "14.517450",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "7.5",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "18.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "15.0",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "AKALA",
                            Name: "Praha 8-Karlín",
                            Owner: "ČHMÚ",
                            Classif: "dopravní",
                            Lat: "50.094238",
                            Lon: "14.442049",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "9.0",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "11.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "17.2",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "AKOBA",
                            Name: "Praha 8-Kobylisy",
                            Owner: "ČHMÚ",
                            Classif: "předměstská",
                            Lat: "50.122189",
                            Lon: "14.467578",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "7.1",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "7.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "92.4",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "15.3",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "ALEGA",
                            Name: "Praha 2-Legerova (hot spot)",
                            Owner: "ČHMÚ",
                            Classif: "dopravní",
                            Lat: "50.072388",
                            Lon: "14.430673",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                    Val: "529",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "18.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "19.3",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "9.0",
                                },
                            ],
                        },
                        {
                            Code: "ALERA",
                            Name: "Letiště Praha",
                            Owner: "Letiště Pr",
                            Classif: "dopravní",
                            Lat: "50.107086",
                            Lon: "14.257226",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "7.3",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                    Val: "196",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "14.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "99.6",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "19.5",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "9.0",
                                },
                            ],
                        },
                        {
                            Code: "ALIBA",
                            Name: "Praha 4-Libuš",
                            Owner: "ČHMÚ",
                            Classif: "předměstská",
                            Lat: "50.007305",
                            Lon: "14.445933",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "2.7",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "8.8",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                    Val: "393",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "18.4",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "102.9",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "17.1",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "6.4",
                                },
                            ],
                        },
                        {
                            Code: "APRUA",
                            Name: "Praha 10-Průmyslová",
                            Owner: "ČHMÚ",
                            Classif: "dopravní",
                            Lat: "50.062298",
                            Lon: "14.537820",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "33.7",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "7.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "20.4",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "AREPA",
                            Name: "Praha 1-n. Republiky",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "50.088066",
                            Lon: "14.429220",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "22.6",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "23.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "20.0",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "ARERA",
                            Name: "Praha 5-Řeporyje",
                            Owner: "ZÚ Ústí nL",
                            Classif: "předměstská",
                            Lat: "50.030514",
                            Lon: "14.309583",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "8.4",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "14.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "16.5",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "10.0",
                                },
                            ],
                        },
                        {
                            Code: "ARIEA",
                            Name: "Praha 2-Riegrovy sady",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "50.081482",
                            Lon: "14.442692",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "1.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "8.8",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "9.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "92.8",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "17.6",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "5.0",
                                },
                            ],
                        },
                        {
                            Code: "ASROA",
                            Name: "Praha 10-Šrobárova",
                            Owner: "ZÚUstí/SZÚ",
                            Classif: "městská",
                            Lat: "50.075150",
                            Lon: "14.472661",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "11.7",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "10.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "13.8",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "7.0",
                                },
                            ],
                        },
                        {
                            Code: "ASTOA",
                            Name: "Praha 5-Stodůlky",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "50.046131",
                            Lon: "14.331413",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "16.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "101.7",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "15.8",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "11.0",
                                },
                            ],
                        },
                        {
                            Code: "ASUCA",
                            Name: "Praha 6-Suchdol",
                            Owner: "ČHMÚ",
                            Classif: "předměstská",
                            Lat: "50.126530",
                            Lon: "14.384639",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "7.8",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "12.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "100.9",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "18.3",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "AVRSA",
                            Name: "Praha 10-Vršovice",
                            Owner: "ČHMÚ",
                            Classif: "dopravní",
                            Lat: "50.066429",
                            Lon: "14.446152",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "9.8",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "17.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "17.6",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "AVYNA",
                            Name: "Praha 9-Vysočany",
                            Owner: "ČHMÚ",
                            Classif: "dopravní",
                            Lat: "50.111080",
                            Lon: "14.503096",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "36.9",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "2.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "75.8",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "17.2",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                    ],
                },
                {
                    Name: "Středočeský",
                    Code: "S",
                    Stations: [
                        {
                            Code: "SBERA",
                            Name: "Beroun",
                            Owner: "ČHMÚ",
                            Classif: "dopravní",
                            Lat: "49.957928",
                            Lon: "14.058300",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "9.9",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                    Val: "291",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "16.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "18.6",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "10.0",
                                },
                            ],
                        },
                        {
                            Code: "SKHOA",
                            Name: "Kutná Hora-Orebitská",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "49.950474",
                            Lon: "15.260361",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "7.8",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "13.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "16.7",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "10.0",
                                },
                            ],
                        },
                        {
                            Code: "SKLMA",
                            Name: "Kladno-střed města",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "50.143860",
                            Lon: "14.101784",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "6.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "94.2",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "17.0",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "10.0",
                                },
                            ],
                        },
                        {
                            Code: "SKLSA",
                            Name: "Kladno-Švermov",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "50.167412",
                            Lon: "14.106048",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "1.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "13.2",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "34.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "20.5",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "SKRPA",
                            Name: "Kralupy nad Vltavou-sportoviště",
                            Owner: "ZÚ Ústí nL",
                            Classif: "průmyslová",
                            Lat: "50.251415",
                            Lon: "14.316583",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "10.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "14.7",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "7.0",
                                },
                            ],
                        },
                        {
                            Code: "SMBOA",
                            Name: "Mladá Boleslav",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "50.428646",
                            Lon: "14.913859",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "7.3",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "7.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "94.4",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "13.7",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "11.0",
                                },
                            ],
                        },
                        {
                            Code: "SONRA",
                            Name: "Ondřejov",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "49.913513",
                            Lon: "14.782625",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "89.6",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "SPBRA",
                            Name: "Příbram-Březové Hory",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "49.676315",
                            Lon: "13.991222",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "9.9",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "16.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "20.5",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "SRORA",
                            Name: "Rožďalovice-Ruská",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "50.301983",
                            Lon: "15.178303",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "1.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "3.8",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "7.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "13.9",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "11.0",
                                },
                            ],
                        },
                        {
                            Code: "STCSA",
                            Name: "Tobolka-Čertovy schody",
                            Owner: "VČs",
                            Classif: "venkovská",
                            Lat: "49.918503",
                            Lon: "14.094489",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "3.3",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                    Val: "174",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "100.5",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "13.0",
                                },
                            ],
                        },
                    ],
                },
                {
                    Name: "Jihočeský",
                    Code: "C",
                    Stations: [
                        {
                            Code: "CCBDA",
                            Name: "České Budějovice",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "48.984386",
                            Lon: "14.465684",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "4.8",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "9.2",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "16.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "80.4",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "14.8",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "15.0",
                                },
                            ],
                        },
                        {
                            Code: "CCBTA",
                            Name: "Čes. Budějovice-Třešň.",
                            Owner: "ZÚ Ústí nL",
                            Classif: "městská",
                            Lat: "48.965904",
                            Lon: "14.508792",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "7.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "10.2",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "7.0",
                                },
                            ],
                        },
                        {
                            Code: "CCHUA",
                            Name: "Churáňov",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "49.068436",
                            Lon: "13.614801",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "94.0",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "CKOCA",
                            Name: "Kocelovice",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "49.467274",
                            Lon: "13.837456",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "98.2",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "CPRAA",
                            Name: "Prachatice",
                            Owner: "ČHMÚ",
                            Classif: "předměstská",
                            Lat: "49.016087",
                            Lon: "14.000444",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "6.9",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "5.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "80.6",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "14.8",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "CTABA",
                            Name: "Tábor",
                            Owner: "ČHMÚ",
                            Classif: "dopravní",
                            Lat: "49.411232",
                            Lon: "14.676389",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "9.0",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                    Val: "274",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "23.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "62.4",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "17.1",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "CHVOA",
                            Name: "Hojná Voda",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "48.724197",
                            Lon: "14.723382",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "10.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "88.4",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "12.3",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                    ],
                },
                {
                    Name: "Plzeňský",
                    Code: "P",
                    Stations: [
                        {
                            Code: "PKLSA",
                            Name: "Klatovy soud",
                            Owner: "ZÚ Ústí nL",
                            Classif: "dopravní",
                            Lat: "49.400608",
                            Lon: "13.286923",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "14.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "15.6",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "11.0",
                                },
                            ],
                        },
                        {
                            Code: "PKUJA",
                            Name: "Kamenný Újezd",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "49.722000",
                            Lon: "13.618538",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "1.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "5.7",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "21.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "16.9",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "14.0",
                                },
                            ],
                        },
                        {
                            Code: "PPLAA",
                            Name: "Plzeň-Slovany",
                            Owner: "MPl",
                            Classif: "dopravní",
                            Lat: "49.732449",
                            Lon: "13.402281",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "5.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "15.9",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                    Val: "185",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "22.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "62.8",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "15.5",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "3.0",
                                },
                            ],
                        },
                        {
                            Code: "PPLEA",
                            Name: "Plzeň-střed",
                            Owner: "MPl",
                            Classif: "dopravní",
                            Lat: "49.747330",
                            Lon: "13.381039",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "2.9",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "17.8",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "14.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "15.0",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "10.0",
                                },
                            ],
                        },
                        {
                            Code: "PPLLA",
                            Name: "Plzeň-Lochotín",
                            Owner: "MPl",
                            Classif: "městská",
                            Lat: "49.770126",
                            Lon: "13.368221",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "9.6",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "12.2",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "15.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "74.4",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "15.7",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "11.0",
                                },
                            ],
                        },
                        {
                            Code: "PPLRA",
                            Name: "Plzeň-Roudná",
                            Owner: "ZÚ Ústí nL",
                            Classif: "městská",
                            Lat: "49.761787",
                            Lon: "13.381614",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "20.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "16.9",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "11.0",
                                },
                            ],
                        },
                        {
                            Code: "PPLVA",
                            Name: "Plzeň-Doubravka",
                            Owner: "ČHMÚ",
                            Classif: "předměstská",
                            Lat: "49.768616",
                            Lon: "13.423381",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "1.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "4.8",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "13.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "83.4",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "15.2",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "7.0",
                                },
                            ],
                        },
                        {
                            Code: "PPMOA",
                            Name: "Plzeň - mobil 28.01.2020 Plzeň - U Velkého rybníka",
                            Owner: "MPl",
                            Classif: "",
                            Lat: "49.770443",
                            Lon: "13.399476",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "4.8",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "6.1",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                    Val: "249",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "14.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "70.8",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "14.9",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "10.0",
                                },
                            ],
                        },
                        {
                            Code: "PPRMA",
                            Name: "Přimda",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "49.669582",
                            Lon: "12.677884",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "2.9",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "2.1",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "97.8",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                    ],
                },
                {
                    Name: "Karlovarský",
                    Code: "K",
                    Stations: [
                        {
                            Code: "KCHMA",
                            Name: "Cheb",
                            Owner: "ČHMÚ",
                            Classif: "předměstská",
                            Lat: "50.065861",
                            Lon: "12.363442",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "16.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "15.2",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "KPRBA",
                            Name: "Přebuz",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "50.372478",
                            Lon: "12.615380",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "KSOMA",
                            Name: "Sokolov",
                            Owner: "ČHMÚ",
                            Classif: "předměstská",
                            Lat: "50.172825",
                            Lon: "12.672818",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "1.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "15.7",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "13.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "89.8",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "14.3",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "10.0",
                                },
                            ],
                        },
                    ],
                },
                {
                    Name: "Ústecký",
                    Code: "U",
                    Stations: [
                        {
                            Code: "UDCMA",
                            Name: "Děčín",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "50.774151",
                            Lon: "14.218794",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "15.7",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "9.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "18.5",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "11.0",
                                },
                            ],
                        },
                        {
                            Code: "UDOKA",
                            Name: "Doksany",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "50.458855",
                            Lon: "14.170162",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "1.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "6.3",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "99.2",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "UCHMA",
                            Name: "Chomutov",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "50.467529",
                            Lon: "13.412696",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "8.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "13.8",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "UKRUA",
                            Name: "Krupka",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "50.696671",
                            Lon: "13.847692",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "2.9",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "9.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "10.2",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "ULOMA",
                            Name: "Lom",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "50.585766",
                            Lon: "13.673418",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "1.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "4.0",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "11.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "91.8",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "21.4",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "12.0",
                                },
                            ],
                        },
                        {
                            Code: "ULTTA",
                            Name: "Litoměřice",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "50.540897",
                            Lon: "14.119409",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "3.5",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "10.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "96.8",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "13.9",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "UMEDA",
                            Name: "Měděnec",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "50.427589",
                            Lon: "13.130143",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "1.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "11.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "12.6",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "UMOMA",
                            Name: "Most",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "50.510365",
                            Lon: "13.645272",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "7.8",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "19.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "95.6",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "24.7",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "8.0",
                                },
                            ],
                        },
                        {
                            Code: "URVHA",
                            Name: "Rudolice v Horách",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "50.579834",
                            Lon: "13.419506",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "11.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "102.7",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "11.1",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "USNZA",
                            Name: "Sněžník",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "50.789444",
                            Lon: "14.086799",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "1.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "3.8",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "99.2",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "USTEA",
                            Name: "Štětí",
                            Owner: "MSTE",
                            Classif: "městská",
                            Lat: "50.454155",
                            Lon: "14.374163",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "1.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "8.4",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "15.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "94.2",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "16.9",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "8.0",
                                },
                            ],
                        },
                        {
                            Code: "UTPMA",
                            Name: "Teplice ",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "50.645279",
                            Lon: "13.851250",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "1.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "15.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "97.6",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "14.4",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "14.0",
                                },
                            ],
                        },
                        {
                            Code: "UTUSA",
                            Name: "Tušimice",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "50.376587",
                            Lon: "13.327622",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "1.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "5.2",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "15.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "100.3",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "22.9",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "17.0",
                                },
                            ],
                        },
                        {
                            Code: "UUDIA",
                            Name: "Ústí n. L.-Prokopa Diviše",
                            Owner: "ZÚ Ústí nL",
                            Classif: "průmyslová",
                            Lat: "50.662979",
                            Lon: "14.031243",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "12.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "14.6",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "6.0",
                                },
                            ],
                        },
                        {
                            Code: "UULDA",
                            Name: "Ústí n.L.-Všebořická (hot spot)",
                            Owner: "ČHMÚ",
                            Classif: "dopravní",
                            Lat: "50.683125",
                            Lon: "13.997873",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "35.4",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                    Val: "198",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "14.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "14.1",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "7.0",
                                },
                            ],
                        },
                        {
                            Code: "UULKA",
                            Name: "Ústí n.L.-Kočkov",
                            Owner: "ČHMÚ",
                            Classif: "předměstská",
                            Lat: "50.683525",
                            Lon: "14.041195",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "1.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "3.4",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "19.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "102.1",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "12.0",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "8.0",
                                },
                            ],
                        },
                        {
                            Code: "UULMA",
                            Name: "Ústí n.L.-město",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "50.661095",
                            Lon: "14.043063",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "8.4",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "13.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "16.4",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                    ],
                },
                {
                    Name: "Liberecký",
                    Code: "L",
                    Stations: [
                        {
                            Code: "LCLMA",
                            Name: "Česká Lípa",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "50.698044",
                            Lon: "14.537345",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "11.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "12.9",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "LFRTA",
                            Name: "Frýdlant",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "50.940651",
                            Lon: "15.069817",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "1.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "99.2",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "LLILA",
                            Name: "Liberec Rochlice",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "50.755100",
                            Lon: "15.069967",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "1.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "6.5",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "12.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "99.8",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "18.3",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "9.0",
                                },
                            ],
                        },
                        {
                            Code: "LSOUA",
                            Name: "Souš",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "50.789646",
                            Lon: "15.319683",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "99.9",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                    ],
                },
                {
                    Name: "Královéhradecký",
                    Code: "H",
                    Stations: [
                        {
                            Code: "HHKBA",
                            Name: "Hradec Králové-Brněnská",
                            Owner: "ČHMÚ",
                            Classif: "dopravní",
                            Lat: "50.195362",
                            Lon: "15.846376",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "18.7",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                    Val: "242",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "14.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "17.5",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "15.0",
                                },
                            ],
                        },
                        {
                            Code: "HHKOK",
                            Name: "Hradec Králové-observatoř",
                            Owner: "ČHMÚ",
                            Classif: "předměstská",
                            Lat: "50.177631",
                            Lon: "15.838390",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "101.5",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "HHKSA",
                            Name: "Hr.Král.-Sukovy sady",
                            Owner: "ZÚ Ústí nL",
                            Classif: "dopravní",
                            Lat: "50.211700",
                            Lon: "15.814149",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "15.1",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "14.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "16.4",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "11.0",
                                },
                            ],
                        },
                        {
                            Code: "HKRYA",
                            Name: "Krkonoše-Rýchory",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "50.660439",
                            Lon: "15.850090",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "93.2",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "HPLOA",
                            Name: "Polom",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "50.350563",
                            Lon: "16.322727",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "1.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "1.0",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "22.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "99.2",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "17.3",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "HTRTA",
                            Name: "Trutnov - Tkalcovská",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "50.565880",
                            Lon: "15.903927",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "16.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "17.0",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                    ],
                },
                {
                    Name: "Pardubický",
                    Code: "E",
                    Stations: [
                        {
                            Code: "EMTPA",
                            Name: "Moravská Třebová - Piaristická.",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "49.758995",
                            Lon: "16.666721",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "11.3",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "30.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "21.4",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "20.0",
                                },
                            ],
                        },
                        {
                            Code: "EPAOA",
                            Name: "Pardubice-Rosice",
                            Owner: "ČHMÚ",
                            Classif: "předměstská",
                            Lat: "50.042198",
                            Lon: "15.739414",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "EPAUA",
                            Name: "Pardubice Dukla",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "50.024036",
                            Lon: "15.763549",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "4.8",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "26.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "101.5",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "17.8",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "10.0",
                                },
                            ],
                        },
                        {
                            Code: "ESVRA",
                            Name: "Svratouch",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "49.735085",
                            Lon: "16.034197",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "87.6",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                    ],
                },
                {
                    Name: "Vysočina",
                    Code: "J",
                    Stations: [
                        {
                            Code: "JJIHA",
                            Name: "Jihlava",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "49.401596",
                            Lon: "15.610246",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "4.5",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "4.4",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                    Val: "274",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "25.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "98.0",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "17.0",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "22.0",
                                },
                            ],
                        },
                        {
                            Code: "JJIZA",
                            Name: "Jihlava-Znojemská",
                            Owner: "ZÚ-Ostrava",
                            Classif: "dopravní",
                            Lat: "49.392445",
                            Lon: "15.591278",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "38.3",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "9.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "16.8",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "6.0",
                                },
                            ],
                        },
                        {
                            Code: "JKMYA",
                            Name: "Kostelní Myslová",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "49.159153",
                            Lon: "15.439048",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "JKOSA",
                            Name: "Košetice",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "49.573395",
                            Lon: "15.080278",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "2.6",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "2.3",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                    Val: "148",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "15.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "76.4",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "14.7",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "14.0",
                                },
                            ],
                        },
                        {
                            Code: "JPEMA",
                            Name: "Pelhřimov",
                            Owner: "ZÚ-Ostrava",
                            Classif: "předměstská",
                            Lat: "49.435001",
                            Lon: "15.208333",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "12.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "13.5",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "11.0",
                                },
                            ],
                        },
                        {
                            Code: "JTREA",
                            Name: "Třebíč",
                            Owner: "ČHMÚ",
                            Classif: "předměstská",
                            Lat: "49.223438",
                            Lon: "15.865778",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "28.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "18.2",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "JZNZA",
                            Name: "Ždár nad Sázavou",
                            Owner: "ZÚ-Ostrava",
                            Classif: "městská",
                            Lat: "49.564556",
                            Lon: "15.941000",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "9.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "12.8",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "7.0",
                                },
                            ],
                        },
                    ],
                },
                {
                    Name: "Jihomoravský",
                    Code: "B",
                    Stations: [
                        {
                            Code: "BBDNA",
                            Name: "Brno - Dětská nemocnice",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "49.202724",
                            Lon: "16.616287",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "12.6",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "26.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "60.6",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "20.2",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "15.0",
                                },
                            ],
                        },
                        {
                            Code: "BBMAA",
                            Name: "Brno-Arboretum",
                            Owner: "SMBrno",
                            Classif: "městská",
                            Lat: "49.216087",
                            Lon: "16.613836",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "10.1",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "15.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "78.0",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "16.9",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "13.6",
                                },
                            ],
                        },
                        {
                            Code: "BBMKA",
                            Name: "Brno-Zvonařka",
                            Owner: "SMBrno",
                            Classif: "průmyslová",
                            Lat: "49.185883",
                            Lon: "16.613661",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "22.8",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                    Val: "306",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "40.2",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "24.9",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "20.3",
                                },
                            ],
                        },
                        {
                            Code: "BBMLA",
                            Name: "Brno-Lány",
                            Owner: "SMBrno",
                            Classif: "předměstská",
                            Lat: "49.165260",
                            Lon: "16.580812",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "1.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "14.3",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                    Val: "291",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "19.5",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "48.5",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "21.7",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "17.0",
                                },
                            ],
                        },
                        {
                            Code: "BBMSA",
                            Name: "Brno-Svatoplukova",
                            Owner: "SMBrno",
                            Classif: "dopravní",
                            Lat: "49.208160",
                            Lon: "16.642517",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "64.8",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "26.6",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "28.0",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "23.4",
                                },
                            ],
                        },
                        {
                            Code: "BBMVA",
                            Name: "Brno-Výstaviště",
                            Owner: "SMBrno",
                            Classif: "dopravní",
                            Lat: "49.189621",
                            Lon: "16.569538",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "33.9",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "16.2",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "17.3",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "12.9",
                                },
                            ],
                        },
                        {
                            Code: "BBNAA",
                            Name: "Brno-Masná",
                            Owner: "ZÚ-Ostrava",
                            Classif: "městská",
                            Lat: "49.188831",
                            Lon: "16.627001",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "19.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "20.5",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "14.0",
                                },
                            ],
                        },
                        {
                            Code: "BBNIA",
                            Name: "Brno-Líšeň",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "49.213211",
                            Lon: "16.678024",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "10.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "19.6",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "11.0",
                                },
                            ],
                        },
                        {
                            Code: "BBNVA",
                            Name: "Brno-Úvoz (hot spot)",
                            Owner: "ČHMÚ",
                            Classif: "dopravní",
                            Lat: "49.198090",
                            Lon: "16.593643",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "58.5",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                    Val: "469",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "26.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "21.9",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "14.0",
                                },
                            ],
                        },
                        {
                            Code: "BBNYA",
                            Name: "Brno-Tuřany",
                            Owner: "ČHMÚ",
                            Classif: "předměstská",
                            Lat: "49.148972",
                            Lon: "16.696217",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "2.9",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "12.2",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "21.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "71.8",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "20.0",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "23.0",
                                },
                            ],
                        },
                        {
                            Code: "BHODA",
                            Name: "Hodonín",
                            Owner: "ZÚ-Ostrava",
                            Classif: "městská",
                            Lat: "48.857277",
                            Lon: "17.131390",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "15.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "18.8",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "11.0",
                                },
                            ],
                        },
                        {
                            Code: "BKUCA",
                            Name: "Kuchařovice",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "48.881355",
                            Lon: "16.085817",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "90.4",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "BMISA",
                            Name: "Mikulov-Sedlec",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "48.791767",
                            Lon: "16.724497",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "18.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "74.2",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "18.5",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "13.0",
                                },
                            ],
                        },
                        {
                            Code: "BMOCA",
                            Name: "Sivice",
                            Owner: "Českomorav",
                            Classif: "venkovská",
                            Lat: "49.208195",
                            Lon: "16.778444",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "21.1",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "",
                                },
                            ],
                        },
                        {
                            Code: "BMOKA",
                            Name: "Mokrá",
                            Owner: "Českomorav",
                            Classif: "venkovská",
                            Lat: "49.219444",
                            Lon: "16.755306",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "",
                                },
                            ],
                        },
                        {
                            Code: "BZNOA",
                            Name: "Znojmo",
                            Owner: "ČHMÚ",
                            Classif: "předměstská",
                            Lat: "48.842957",
                            Lon: "16.060127",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "19.5",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "20.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "20.3",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "17.0",
                                },
                            ],
                        },
                    ],
                },
                {
                    Name: "Olomoucký",
                    Code: "M",
                    Stations: [
                        {
                            Code: "MBELA",
                            Name: "Bělotín",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "49.587082",
                            Lon: "17.804220",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "19.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "22.6",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "14.0",
                                },
                            ],
                        },
                        {
                            Code: "MJESA",
                            Name: "Jeseník-lázně",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "50.242241",
                            Lon: "17.190180",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "4.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "8.0",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "21.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "82.8",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "14.8",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "MLOSA",
                            Name: "Loštice",
                            Owner: "OLOŠ",
                            Classif: "venkovská",
                            Lat: "49.747917",
                            Lon: "16.925777",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "25.5",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "",
                                },
                            ],
                        },
                        {
                            Code: "MOLJA",
                            Name: "Olomouc-Hejčín",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "49.601463",
                            Lon: "17.238073",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "10.1",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "32.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "21.4",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "23.0",
                                },
                            ],
                        },
                        {
                            Code: "MOLSA",
                            Name: "Olomouc-Šmeralova",
                            Owner: "ZÚ-Ostrava",
                            Classif: "městská",
                            Lat: "49.592918",
                            Lon: "17.266167",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "",
                                },
                            ],
                        },
                        {
                            Code: "MPHRA",
                            Name: "Hranice",
                            Owner: "MHRA",
                            Classif: "městská",
                            Lat: "49.551888",
                            Lon: "17.731222",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "23.7",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "",
                                },
                            ],
                        },
                        {
                            Code: "MPRRA",
                            Name: "Přerov",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "49.451656",
                            Lon: "17.454159",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "1.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "21.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "84.4",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "21.4",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "15.0",
                                },
                            ],
                        },
                        {
                            Code: "MPSTA",
                            Name: "Prostějov",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "49.467857",
                            Lon: "17.114725",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "22.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "22.2",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "MSMSA",
                            Name: "Šumperk - 5.ZŠ",
                            Owner: "MŠUM",
                            Classif: "městská",
                            Lat: "49.971584",
                            Lon: "16.978472",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "16.2",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "",
                                },
                            ],
                        },
                    ],
                },
                {
                    Name: "Zlínský",
                    Code: "Z",
                    Stations: [
                        {
                            Code: "ZOTMA",
                            Name: "Otrokovice-město",
                            Owner: "MOTRO",
                            Classif: "dopravní",
                            Lat: "49.208912",
                            Lon: "17.534742",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "28.2",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "ZSNVA",
                            Name: "Štítná n.Vláří",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "49.047817",
                            Lon: "18.007828",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "76.8",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "ZTNVA",
                            Name: "Těšnovice",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "49.259392",
                            Lon: "17.410561",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "5.6",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "5.2",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "20.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "78.2",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "26.4",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "14.0",
                                },
                            ],
                        },
                        {
                            Code: "ZUHRA",
                            Name: "Uherské Hradiště",
                            Owner: "ČHMÚ",
                            Classif: "dopravní",
                            Lat: "49.067951",
                            Lon: "17.466848",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "55.1",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                    Val: "551",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "26.9",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "24.6",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "ZVMZA",
                            Name: "Valašské Meziříčí",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "49.472057",
                            Lon: "17.966976",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "19.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "24.6",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "19.0",
                                },
                            ],
                        },
                        {
                            Code: "ZZLNA",
                            Name: "Zlín",
                            Owner: "ČHMÚ",
                            Classif: "předměstská",
                            Lat: "49.232906",
                            Lon: "17.667175",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "1.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "9.4",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "21.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "71.8",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "18.7",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "10.0",
                                },
                            ],
                        },
                        {
                            Code: "ZZZSA",
                            Name: "Zlín - ZŠ Kvítkova",
                            Owner: "MZLl",
                            Classif: "městská",
                            Lat: "49.228474",
                            Lon: "17.675083",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "11.7",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "19.6",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "62.0",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "22.2",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "17.6",
                                },
                            ],
                        },
                    ],
                },
                {
                    Name: "Moravskoslezský",
                    Code: "T",
                    Stations: [
                        {
                            Code: "TBKRA",
                            Name: "Bílý Kříž",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "49.502609",
                            Lon: "18.538561",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "1.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "1.9",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "76.8",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "TCERA",
                            Name: "Červená hora",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "49.777142",
                            Lon: "17.541946",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "5.5",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "90.8",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "TCTNA",
                            Name: "Český Těšín",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "49.748959",
                            Lon: "18.609726",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "13.8",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "18.9",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "29.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "32.3",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "15.0",
                                },
                            ],
                        },
                        {
                            Code: "TFMIA",
                            Name: "Frýdek-Místek",
                            Owner: "ČHMÚ",
                            Classif: "předměstská",
                            Lat: "49.671791",
                            Lon: "18.351070",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "14.0",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "15.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "28.0",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "14.0",
                                },
                            ],
                        },
                        {
                            Code: "THAOA",
                            Name: "Havířov",
                            Owner: "ZÚ, SMHa",
                            Classif: "městská",
                            Lat: "49.771526",
                            Lon: "18.443193",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "9.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "THARA",
                            Name: "Havířov",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "49.790977",
                            Lon: "18.406836",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "15.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "31.6",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "12.0",
                                },
                            ],
                        },
                        {
                            Code: "TCHOA",
                            Name: "Chotěbuz",
                            Owner: "ZÚ, MSK",
                            Classif: "dopravní",
                            Lat: "49.778008",
                            Lon: "18.599783",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "5.6",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "4.0",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                    Val: "433",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "10.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "22.7",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "TKAOK",
                            Name: "Karviná-ZÚ",
                            Owner: "ZÚ-Ostrava",
                            Classif: "dopravní",
                            Lat: "49.858891",
                            Lon: "18.557777",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "20.9",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "18.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "32.1",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "13.0",
                                },
                            ],
                        },
                        {
                            Code: "TKARA",
                            Name: "Karviná",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "49.863796",
                            Lon: "18.551453",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "2.7",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "11.1",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "22.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "80.2",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "26.9",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "10.0",
                                },
                            ],
                        },
                        {
                            Code: "TNSVA",
                            Name: "Nošovice",
                            Owner: "ONOS",
                            Classif: "venkovská",
                            Lat: "49.653072",
                            Lon: "18.431831",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "13.2",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "12.7",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "30.6",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "11.0",
                                },
                            ],
                        },
                        {
                            Code: "TOCBA",
                            Name: "Ostrava-Českobratrská (hot spot)",
                            Owner: "ČHMÚ",
                            Classif: "dopravní",
                            Lat: "49.839848",
                            Lon: "18.289976",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "46.9",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                    Val: "671",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "15.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "40.9",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "11.0",
                                },
                            ],
                        },
                        {
                            Code: "TOFFA",
                            Name: "Ostrava-Fifejdy",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "49.839188",
                            Lon: "18.263689",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "1.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "9.0",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "10.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "87.8",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "36.4",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "TOHOA",
                            Name: "Ostrava Hošťálkovice",
                            Owner: "ZÚ, MSK",
                            Classif: "předměstská",
                            Lat: "49.861393",
                            Lon: "18.213346",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "5.6",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "23.5",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                    Val: "320",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "10.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "23.8",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "TOHUA",
                            Name: "Ostrava - Hrušov",
                            Owner: "ZÚ, SMOva",
                            Classif: "průmyslová",
                            Lat: "49.867722",
                            Lon: "18.283751",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "14.9",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "38.4",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "11.5",
                                },
                            ],
                        },
                        {
                            Code: "TOKOA",
                            Name: "Opava-Komárov",
                            Owner: "ZÚ, MSK",
                            Classif: "předměstská",
                            Lat: "49.915268",
                            Lon: "17.965715",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "5.6",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "16.6",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                    Val: "467",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "21.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "27.4",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "TOMHK",
                            Name: "Ostrava-Mariánské Hory",
                            Owner: "ZÚ, SMOva",
                            Classif: "průmyslová",
                            Lat: "49.824860",
                            Lon: "18.263655",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "5.6",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "10.7",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                    Val: "319",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "13.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "93.6",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "23.6",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "TOPDA",
                            Name: "Ostrava-Poruba, DD",
                            Owner: "ZÚ, SMOva",
                            Classif: "dopravní",
                            Lat: "49.835472",
                            Lon: "18.165222",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "15.7",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "15.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "34.5",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "13.0",
                                },
                            ],
                        },
                        {
                            Code: "TOPOA",
                            Name: "Ostrava-Poruba/ČHMÚ",
                            Owner: "ČHMÚ",
                            Classif: "předměstská",
                            Lat: "49.825294",
                            Lon: "18.159275",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "7.7",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "18.2",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "16.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "26.7",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "TOPRA",
                            Name: "Ostrava-Přívoz",
                            Owner: "ČHMÚ",
                            Classif: "průmyslová",
                            Lat: "49.856258",
                            Lon: "18.269741",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "23.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "34.5",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "20.0",
                                },
                            ],
                        },
                        {
                            Code: "TOREK",
                            Name: "Ostrava-Radvanice ZÚ",
                            Owner: "ZÚ, SMOva",
                            Classif: "průmyslová",
                            Lat: "49.807056",
                            Lon: "18.339140",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "5.6",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "11.7",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                    Val: "267",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "15.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "78.2",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "30.5",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "13.0",
                                },
                            ],
                        },
                        {
                            Code: "TOROK",
                            Name: "Ostrava-Radvanice OZO",
                            Owner: "ZÚ, SMOva",
                            Classif: "předměstská",
                            Lat: "49.818558",
                            Lon: "18.340389",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "5.6",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "8.2",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "8.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "86.2",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "26.1",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                },
                            ],
                        },
                        {
                            Code: "TOVKA",
                            Name: "Opava-Kateřinky",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "49.944988",
                            Lon: "17.909531",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "7.8",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "33.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "84.8",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "29.5",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "31.0",
                                },
                            ],
                        },
                        {
                            Code: "TOZRA",
                            Name: "Ostrava-Zábřeh",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "49.796040",
                            Lon: "18.247181",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "34.0",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "",
                                },
                            ],
                        },
                        {
                            Code: "TRYCA",
                            Name: "Rychvald",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "49.871670",
                            Lon: "18.377254",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "3.5",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "5.5",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "14.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "30.4",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "7.0",
                                },
                            ],
                        },
                        {
                            Code: "TSTDA",
                            Name: "Studénka",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "49.720936",
                            Lon: "18.089306",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "1.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "5.9",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                    Val: "338",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "8.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "85.0",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "25.5",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "10.0",
                                },
                            ],
                        },
                        {
                            Code: "TTRKA",
                            Name: "Třinec-Kanada",
                            Owner: "SMTř.",
                            Classif: "předměstská",
                            Lat: "49.672379",
                            Lon: "18.643038",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "22.0",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "21.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "26.4",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "15.0",
                                },
                            ],
                        },
                        {
                            Code: "TTROA",
                            Name: "Třinec-Kosmos",
                            Owner: "ČHMÚ",
                            Classif: "městská",
                            Lat: "49.668114",
                            Lon: "18.677799",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "26.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                    Val: "70.4",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "25.2",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "16.0",
                                },
                            ],
                        },
                        {
                            Code: "TVERA",
                            Name: "Věřňovice",
                            Owner: "ČHMÚ",
                            Classif: "venkovská",
                            Lat: "49.924679",
                            Lon: "18.422873",
                            Components: [
                                {
                                    Code: "SO2",
                                    Int: "1h",
                                    Val: "1.3",
                                },
                                {
                                    Code: "NO2",
                                    Int: "1h",
                                    Val: "4.2",
                                },
                                {
                                    Code: "CO",
                                    Int: "8h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "1h",
                                    Val: "23.0",
                                },
                                {
                                    Code: "O3",
                                    Int: "1h",
                                },
                                {
                                    Code: "PM10",
                                    Int: "24h",
                                    Val: "30.7",
                                },
                                {
                                    Code: "PM2_5",
                                    Int: "1h",
                                    Val: "15.0",
                                },
                            ],
                        },
                    ],
                },
            ],
        },
    ],
};
