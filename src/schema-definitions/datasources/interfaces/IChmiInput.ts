interface IComponent {
    Code: string;
    Int: string;
    Flag?: string;
    Val?: string;
}

interface IStation {
    Code: string;
    Name: string;
    Owner?: string;
    Classif?: string;
    Lat?: string;
    Lon?: string;
    Ix?: string;
    Components?: IComponent[];
}

interface IRegion {
    Name: string;
    Code: string;
    Stations: IStation[];
}

interface IState {
    Name: string;
    Code: string;
    DateFromTo: string;
    DateFromUTC: string;
    DateToUTC: string;
    Regions: IRegion[];
}

export interface IChmiInput {
    Actualized: string;
    States: IState[];
}
