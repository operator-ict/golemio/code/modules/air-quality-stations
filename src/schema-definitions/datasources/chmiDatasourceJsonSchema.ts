import { IChmiInput } from "#sch/datasources/interfaces/IChmiInput";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export const chmiDatasourceJsonSchema: JSONSchemaType<IChmiInput> = {
    type: "object",
    required: ["Actualized", "States"],
    additionalProperties: true,
    properties: {
        Actualized: { type: "string" },
        States: {
            type: "array",
            items: {
                type: "object",
                required: ["Name", "Code", "DateFromTo", "DateFromUTC", "DateToUTC", "Regions"],
                additionalProperties: false,
                properties: {
                    Name: { type: "string" },
                    Code: { type: "string" },
                    DateFromTo: { type: "string" },
                    DateFromUTC: { type: "string" },
                    DateToUTC: { type: "string" },
                    Regions: {
                        type: "array",
                        items: {
                            type: "object",
                            required: ["Name", "Code", "Stations"],
                            additionalProperties: false,
                            properties: {
                                Name: { type: "string" },
                                Code: { type: "string" },
                                Stations: {
                                    type: "array",
                                    items: {
                                        type: "object",
                                        required: ["Code", "Name"],
                                        additionalProperties: false,
                                        properties: {
                                            Code: { type: "string" },
                                            Name: { type: "string" },
                                            Owner: { type: "string", nullable: true },
                                            Classif: { type: "string", nullable: true },
                                            Lat: { type: "string", nullable: true },
                                            Lon: { type: "string", nullable: true },
                                            Ix: { type: "string", nullable: true },
                                            Components: {
                                                type: "array",
                                                nullable: true,
                                                items: {
                                                    type: "object",
                                                    required: ["Code", "Int"],
                                                    additionalProperties: false,
                                                    properties: {
                                                        Code: { type: "string" },
                                                        Int: { type: "string" },
                                                        Flag: { type: "string", nullable: true },
                                                        Val: { type: "string", nullable: true },
                                                    },
                                                },
                                            },
                                        },
                                    },
                                },
                            },
                        },
                    },
                },
            },
        },
    },
};
