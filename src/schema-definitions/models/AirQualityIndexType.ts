import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import IAirQualityIndexType from "./interfaces/IAirQualityIndexType";

export class AirQualityIndexType extends Model<AirQualityIndexType> implements IAirQualityIndexType {
    declare id: number;
    declare index_code: string;
    declare limit_gte: string;
    declare limit_lt: string;
    declare color: string;
    declare color_text: string;
    declare description_cs: string;
    declare description_en: string;

    public static attributeModel: ModelAttributes<AirQualityIndexType> = {
        id: { type: DataTypes.INTEGER, primaryKey: true },
        index_code: { type: DataTypes.TEXT },
        limit_gte: { type: DataTypes.TEXT },
        limit_lt: { type: DataTypes.TEXT },
        color: { type: DataTypes.TEXT },
        color_text: { type: DataTypes.TEXT },
        description_cs: { type: DataTypes.TEXT },
        description_en: { type: DataTypes.TEXT },
    };
}
