import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import IAirQualityComponentType from "./interfaces/IAirQualityComponentType";

export class AirQualityComponentType extends Model<AirQualityComponentType> implements IAirQualityComponentType {
    declare id: number;
    declare component_code: string;
    declare unit: string;
    declare description_cs: string;
    declare description_en: string;

    public static attributeModel: ModelAttributes<AirQualityComponentType> = {
        id: { type: DataTypes.INTEGER, primaryKey: true },
        component_code: { type: DataTypes.TEXT },
        unit: { type: DataTypes.TEXT },
        description_cs: { type: DataTypes.TEXT },
        description_en: { type: DataTypes.TEXT },
    };
}
