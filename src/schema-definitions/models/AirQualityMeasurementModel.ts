import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import Sequelize, { Model, ModelAttributes, ModelIndexesOptions } from "@golemio/core/dist/shared/sequelize";
import IAirQualityMeasurement from "./interfaces/IAirQualityMeasurement";

export class AirQualityMeasurementModel extends Model<IAirQualityMeasurement> implements IAirQualityMeasurement {
    public static tableName = "airqualitystations_measurements";

    declare aggregation_interval: string;
    declare component_code: string;
    declare measured_from: string;
    declare measured_to: string;
    declare station_id: string;
    declare value: number | null;

    public static attributeModel: ModelAttributes<AirQualityMeasurementModel> = {
        aggregation_interval: {
            primaryKey: true,
            type: Sequelize.STRING,
        },
        component_code: {
            primaryKey: true,
            type: Sequelize.STRING,
        },
        measured_from: {
            primaryKey: true,
            type: Sequelize.DATE,
        },
        measured_to: {
            primaryKey: true,
            type: Sequelize.DATE,
        },
        station_id: {
            primaryKey: true,
            type: Sequelize.STRING,
        },
        value: {
            allowNull: true,
            type: Sequelize.DOUBLE(25),
        },
    };

    public static indexes: ModelIndexesOptions[] = [
        {
            unique: false,
            using: "BTREE",
            name: "airqualitystations_measurements_aggregation_interval",
            fields: ["aggregation_interval"],
        },
        {
            unique: false,
            using: "BTREE",
            name: "airqualitystations_measurements_component_code",
            fields: ["component_code"],
        },
    ];

    public static updateAttributeList = Object.keys(AirQualityMeasurementModel.attributeModel).concat("updated_at");

    public static jsonSchema: JSONSchemaType<IAirQualityMeasurement[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                aggregation_interval: {
                    type: "string",
                },
                component_code: {
                    type: "string",
                },
                measured_from: {
                    type: ["string"],
                },
                measured_to: {
                    type: ["string"],
                },
                station_id: {
                    type: "string",
                },
                value: {
                    anyOf: [{ type: "number" }, { type: "null", nullable: true }],
                },
            },
            required: ["aggregation_interval", "component_code", "measured_from", "measured_to", "station_id"],
        },
    };
}
