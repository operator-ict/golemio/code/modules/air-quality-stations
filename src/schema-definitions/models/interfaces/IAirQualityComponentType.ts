export default interface IAirQualityComponentType {
    id: number;
    component_code: string;
    unit: string;
    description_cs: string;
    description_en: string;
}
