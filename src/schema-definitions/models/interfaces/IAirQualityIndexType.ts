export default interface IAirQualityIndexType {
    id: number;
    index_code: string;
    limit_gte: string;
    limit_lt: string;
    color: string;
    color_text: string;
    description_cs: string;
    description_en: string;
}
