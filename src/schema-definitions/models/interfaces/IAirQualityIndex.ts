import IAirQualityMeasurement from "./IAirQualityMeasurement";
import IAirQualityStation from "./IAirQualityStation";

export default interface IAirQualityIndex {
    index_code: string;
    measured_from: string;
    measured_to: string;
    station_id: string;
    updated_at?: string;
    station?: IAirQualityStation;
    measurements?: IAirQualityMeasurement[];
}
