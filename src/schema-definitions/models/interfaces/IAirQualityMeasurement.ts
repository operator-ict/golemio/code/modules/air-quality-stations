export default interface IAirQualityMeasurement {
    aggregation_interval: string;
    component_code: string;
    measured_from: string;
    measured_to: string;
    station_id: string;
    value: number | null;
}
