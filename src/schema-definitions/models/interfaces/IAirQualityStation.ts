export default interface IAirQualityStation {
    classification: string | null;
    id: string;
    latitude: number | null;
    longitude: number | null;
    owner: string | null;
    region_code: string;
    region_name: string;
    state_code: string;
    state_name: string;
    station_name: string;
    station_vendor_id: string;
    district?: string | null;
    updated_at?: string;
}
