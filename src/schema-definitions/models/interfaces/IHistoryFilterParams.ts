export default interface IHistoryFilterParameters {
    from?: string;
    to?: string;
    limit?: number;
    offset?: number;
    sensorId?: string;
}
