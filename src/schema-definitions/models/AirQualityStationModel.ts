import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import Sequelize, { Model, ModelAttributes, ModelIndexesOptions } from "@golemio/core/dist/shared/sequelize";
import IAirQualityStation from "./interfaces/IAirQualityStation";

export class AirQualityStationModel extends Model<IAirQualityStation> implements IAirQualityStation {
    public static tableName = "airqualitystations";

    declare classification: string;
    declare id: string;
    declare latitude: number;
    declare longitude: number;
    declare owner: string;
    declare region_code: string;
    declare region_name: string;
    declare state_code: string;
    declare state_name: string;
    declare station_name: string;
    declare station_vendor_id: string;
    declare district: string | null;
    declare updated_at: string;

    public static attributeModel: ModelAttributes<AirQualityStationModel> = {
        classification: Sequelize.TEXT,
        id: {
            primaryKey: true,
            type: Sequelize.STRING,
        },
        latitude: Sequelize.DOUBLE(25),
        longitude: Sequelize.DOUBLE(25),
        owner: Sequelize.STRING,
        region_code: {
            allowNull: false,
            type: Sequelize.STRING,
        },
        region_name: {
            allowNull: false,
            type: Sequelize.STRING,
        },
        state_code: {
            allowNull: false,
            type: Sequelize.STRING,
        },
        state_name: {
            allowNull: false,
            type: Sequelize.STRING,
        },
        station_name: {
            allowNull: false,
            type: Sequelize.STRING,
        },
        station_vendor_id: {
            allowNull: false,
            type: Sequelize.STRING,
        },
        district: {
            allowNull: true,
            type: Sequelize.STRING,
        },
    };

    public static indexes: ModelIndexesOptions[] = [];

    public static updateAttributeList = Object.keys(AirQualityStationModel.attributeModel)
        .filter((el) => !["id", "district"].includes(el))
        .concat("updated_at");

    public static jsonSchema: JSONSchemaType<IAirQualityStation[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: {
                    type: "string",
                },
                station_name: {
                    type: "string",
                },
                station_vendor_id: {
                    type: "string",
                },
                latitude: {
                    type: "number",
                },
                longitude: {
                    type: "number",
                },
                owner: {
                    type: "string",
                },
                state_name: {
                    type: "string",
                },
                state_code: {
                    type: "string",
                },
                region_name: {
                    type: "string",
                },
                region_code: {
                    type: "string",
                },
                classification: {
                    type: "string",
                },
                district: {
                    nullable: true,
                    oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                },
                updated_at: {
                    type: "string",
                    format: "date-time",
                    nullable: true,
                },
            },
            required: [
                "id",
                "station_name",
                "station_vendor_id",
                "latitude",
                "longitude",
                "owner",
                "state_name",
                "state_code",
                "region_name",
                "region_code",
                "classification",
            ],
        },
    };
}
