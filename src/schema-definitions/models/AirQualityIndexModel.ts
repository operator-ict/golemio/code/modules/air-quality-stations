import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import Sequelize, { Model, ModelAttributes, ModelIndexesOptions } from "@golemio/core/dist/shared/sequelize";
import IAirQualityIndex from "./interfaces/IAirQualityIndex";
import IAirQualityMeasurement from "./interfaces/IAirQualityMeasurement";
import IAirQualityStation from "./interfaces/IAirQualityStation";

type AirQualityIndexWithoutAssociations = Omit<IAirQualityIndex, "station" | "measurements">;

export class AirQualityIndexModel extends Model<AirQualityIndexModel> implements IAirQualityIndex {
    public static tableName = "airqualitystations_indexes";

    declare index_code: string;
    declare measured_from: string;
    declare measured_to: string;
    declare station_id: string;
    declare updated_at: string;
    // associations
    public station?: IAirQualityStation;
    public measurements?: IAirQualityMeasurement[];

    public static attributeModel: ModelAttributes<AirQualityIndexModel> = {
        index_code: {
            allowNull: false,
            type: Sequelize.STRING,
        },
        measured_from: {
            primaryKey: true,
            type: Sequelize.DATE,
        },
        measured_to: {
            primaryKey: true,
            type: Sequelize.DATE,
        },
        station_id: {
            primaryKey: true,
            type: Sequelize.STRING,
        },
    };

    public static indexModel: ModelIndexesOptions[] = [];

    public static updateAttributeList = Object.keys(AirQualityIndexModel.attributeModel).concat("updated_at");

    public static jsonSchema: JSONSchemaType<AirQualityIndexWithoutAssociations[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                index_code: {
                    type: "string",
                },
                measured_from: {
                    type: ["string"],
                },
                measured_to: {
                    type: ["string"],
                },
                station_id: {
                    type: "string",
                },
                updated_at: {
                    type: "string",
                    format: "date-time",
                    nullable: true,
                },
            },
            required: ["index_code", "measured_from", "measured_to", "station_id"],
        },
    };
}
