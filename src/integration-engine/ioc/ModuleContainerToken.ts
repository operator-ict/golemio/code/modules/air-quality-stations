const IEModuleContainerToken = {
    /* datasources */
    Datasource1HData: Symbol(),
    Datasource3HData: Symbol(),
    /* transformations */
    AirQualityStationsTransformation: Symbol(),
    /* repositories */
    StationsRepository: Symbol(),
    MeasurementsRepository: Symbol(),
    IndexesRepository: Symbol(),
    /* services */
    ChmiDataService: Symbol(),
    /* tasks */
    Refresh1HDataInDBTask: Symbol(),
    Refresh3HDataInDBTask: Symbol(),
    UpdateDistrictTask: Symbol(),
};

export { IEModuleContainerToken };
