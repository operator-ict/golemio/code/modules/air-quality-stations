import { IEModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { Datasource1HData } from "#ie/workers/datasources/Datasource1HData";
import { Datasource3HData } from "#ie/workers/datasources/Datasource3HData";
import { IndexesRepository } from "#ie/workers/repositories/IndexesRepository";
import { MeasurementsRepository } from "#ie/workers/repositories/MeasurementsRepository";
import { StationsRepository } from "#ie/workers/repositories/StationsRepository";
import { ChmiDataService } from "#ie/workers/services/ChmiDataService";
import { Refresh1HDataInDBTask } from "#ie/workers/tasks/Refresh1HDataInDBTask";
import { Refresh3HDataInDBTask } from "#ie/workers/tasks/Refresh3HDataInDBTask";
import { UpdateDistrictTask } from "#ie/workers/tasks/UpdateDistrictTask";
import { AirQualityStationsTransformation } from "#ie/workers/transformations/AirQualityStationsTransformation";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";

//#region Initialization
const IEModuleContainer: DependencyContainer = IntegrationEngineContainer.createChildContainer();
//#endregion

//#region Datasources
IEModuleContainer.registerSingleton(IEModuleContainerToken.Datasource1HData, Datasource1HData).registerSingleton(
    IEModuleContainerToken.Datasource3HData,
    Datasource3HData
);
//#endregion

//#region Repositories
IEModuleContainer.registerSingleton(IEModuleContainerToken.StationsRepository, StationsRepository)
    .registerSingleton(IEModuleContainerToken.MeasurementsRepository, MeasurementsRepository)
    .registerSingleton(IEModuleContainerToken.IndexesRepository, IndexesRepository);
//#endregion

//#region Service
IEModuleContainer.register(IEModuleContainerToken.ChmiDataService, ChmiDataService);
//#endregion

//#region Transformations
IEModuleContainer.registerSingleton(IEModuleContainerToken.AirQualityStationsTransformation, AirQualityStationsTransformation);
//#endregion

//#region Tasks
IEModuleContainer.registerSingleton(IEModuleContainerToken.Refresh1HDataInDBTask, Refresh1HDataInDBTask)
    .registerSingleton(IEModuleContainerToken.Refresh3HDataInDBTask, Refresh3HDataInDBTask)
    .registerSingleton(IEModuleContainerToken.UpdateDistrictTask, UpdateDistrictTask);
//#endregion

export { IEModuleContainer };
