import { IEModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { WORKER_NAME } from "#ie/workers/constants";
import { Datasource1HData } from "#ie/workers/datasources/Datasource1HData";
import { ChmiDataService } from "#ie/workers/services/ChmiDataService";
import { AirQualityStationsTransformation } from "#ie/workers/transformations/AirQualityStationsTransformation";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class Refresh1HDataInDBTask extends AbstractEmptyTask {
    public readonly queueName = "refresh1HDataInDB";
    public readonly queueTtl = 59 * 60 * 1000; // 59 minutes

    constructor(
        @inject(IEModuleContainerToken.Datasource1HData) private datasource: Datasource1HData,
        @inject(IEModuleContainerToken.AirQualityStationsTransformation) private transformation: AirQualityStationsTransformation,
        @inject(IEModuleContainerToken.ChmiDataService) private chmiDataService: ChmiDataService
    ) {
        super(WORKER_NAME);
    }

    protected async execute() {
        const data = await this.datasource.getDataSource().getAll();
        const transformedData = this.transformation.transformElement(data);
        await this.chmiDataService.saveProcessedData(transformedData);
    }
}
