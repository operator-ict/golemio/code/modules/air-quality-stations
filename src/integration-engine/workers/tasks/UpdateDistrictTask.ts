import { IEModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { WORKER_NAME } from "#ie/workers/constants";
import { StationsRepository } from "#ie/workers/repositories/StationsRepository";
import { IUpdateDistrictParams, UpdateDistrictValidationSchema } from "#ie/workers/tasks/schemas/UpdateDistrictValidationSchema";
import CityDistrictsRepository from "@golemio/city-districts/dist/integration-engine/repositories/CityDistrictPostgresRepository";
import { AbstractTask } from "@golemio/core/dist/integration-engine";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class UpdateDistrictTask extends AbstractTask<IUpdateDistrictParams> {
    public readonly queueName = "updateDistrict";
    public readonly queueTtl = 23 * 60 * 60 * 1000; // 23 hours
    public readonly schema = UpdateDistrictValidationSchema;

    private cityDistrictsRepository: CityDistrictsRepository;

    constructor(@inject(IEModuleContainerToken.StationsRepository) private stationsRepository: StationsRepository) {
        super(WORKER_NAME);
        this.cityDistrictsRepository = new CityDistrictsRepository();
    }

    protected async execute(data: IUpdateDistrictParams) {
        const dbData = await this.stationsRepository.findOne({
            where: { id: data.id },
        });

        if (!dbData.district) {
            dbData.district = await this.cityDistrictsRepository.getDistrict(dbData.longitude, dbData.latitude);
            dbData.save();
        }
    }
}
