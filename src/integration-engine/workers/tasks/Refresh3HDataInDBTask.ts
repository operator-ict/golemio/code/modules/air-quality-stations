import { IEModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { WORKER_NAME } from "#ie/workers/constants";
import { Datasource3HData } from "#ie/workers/datasources/Datasource3HData";
import { StationsRepository } from "#ie/workers/repositories/StationsRepository";
import { ChmiDataService } from "#ie/workers/services/ChmiDataService";
import { AirQualityStationsTransformation } from "#ie/workers/transformations/AirQualityStationsTransformation";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { AbstractEmptyTask, QueueManager } from "@golemio/core/dist/integration-engine";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class Refresh3HDataInDBTask extends AbstractEmptyTask {
    public readonly queueName = "refresh3HDataInDB";
    public readonly queueTtl = 59 * 60 * 1000; // 59 minutes

    constructor(
        @inject(CoreToken.SimpleConfig) private config: ISimpleConfig,
        @inject(IEModuleContainerToken.Datasource3HData) private datasource: Datasource3HData,
        @inject(IEModuleContainerToken.AirQualityStationsTransformation) private transformation: AirQualityStationsTransformation,
        @inject(IEModuleContainerToken.StationsRepository) private stationsRepository: StationsRepository,
        @inject(IEModuleContainerToken.ChmiDataService) private chmiDataService: ChmiDataService
    ) {
        super(WORKER_NAME);
    }

    protected async execute() {
        const data = await this.datasource.getDataSource().getAll();
        const transformedData = this.transformation.transformElement(data);
        await this.chmiDataService.saveProcessedData(transformedData);

        await this.prepareQueueForDistrictUpdate();
    }

    private prepareQueueForDistrictUpdate = async (): Promise<void> => {
        const result = await this.stationsRepository["sequelizeModel"].findAll({
            where: { district: null, region_name: "Praha" },
        });

        const stationIds = result.map((station) => station.id);
        const exchange = this.config.getValue<string>("env.RABBIT_EXCHANGE_NAME");

        for (const stationId of stationIds) {
            await QueueManager.sendMessageToExchange(exchange + "." + this.queuePrefix, "updateDistrict", { id: stationId });
        }
    };
}
