import { IsString } from "@golemio/core/dist/shared/class-validator";

export interface IUpdateDistrictParams {
    id: string;
}

export class UpdateDistrictValidationSchema implements IUpdateDistrictParams {
    @IsString()
    id!: string;
}
