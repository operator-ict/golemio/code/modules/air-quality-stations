import { IEModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { IndexesRepository } from "#ie/workers/repositories/IndexesRepository";
import { MeasurementsRepository } from "#ie/workers/repositories/MeasurementsRepository";
import { StationsRepository } from "#ie/workers/repositories/StationsRepository";
import { TransformOut } from "#ie/workers/transformations/AirQualityStationsTransformation";
import { AirQualityIndexModel, AirQualityMeasurementModel, AirQualityStationModel } from "#sch/models";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class ChmiDataService {
    constructor(
        @inject(CoreToken.PostgresConnector) private databaseConnector: IDatabaseConnector,
        @inject(IEModuleContainerToken.StationsRepository) private stationsRepository: StationsRepository,
        @inject(IEModuleContainerToken.MeasurementsRepository) private measurementsRepository: MeasurementsRepository,
        @inject(IEModuleContainerToken.IndexesRepository) private indexesRepository: IndexesRepository
    ) {}

    public async saveProcessedData(transformedData: TransformOut) {
        const connection = this.databaseConnector.getConnection();
        const t = await connection.transaction();

        try {
            await this.stationsRepository.bulkSave(
                transformedData.stations,
                AirQualityStationModel.updateAttributeList,
                false,
                false,
                t
            );
            await this.measurementsRepository.bulkSave(
                transformedData.measurements,
                AirQualityMeasurementModel.updateAttributeList,
                false,
                false,
                t
            );
            await this.indexesRepository.bulkSave(
                transformedData.indexes,
                AirQualityIndexModel.updateAttributeList,
                false,
                false,
                t
            );
            await t.commit();
        } catch (err) {
            await t.rollback();
            throw new GeneralError(`Database error: ChmiDataService failed to save data`, this.constructor.name, err);
        }
    }
}
