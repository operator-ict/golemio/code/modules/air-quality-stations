import { IChmiInput } from "#sch/datasources/interfaces/IChmiInput";
import IAirQualityIndex from "#sch/models/interfaces/IAirQualityIndex";
import IAirQualityMeasurement from "#sch/models/interfaces/IAirQualityMeasurement";
import IAirQualityStation from "#sch/models/interfaces/IAirQualityStation";
import { dateTime } from "@golemio/core/dist/helpers";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";

type TransformIn = IChmiInput;
export type TransformOut = {
    indexes: IAirQualityIndex[];
    measurements: IAirQualityMeasurement[];
    stations: IAirQualityStation[];
};

export class AirQualityStationsTransformation extends AbstractTransformation<TransformIn, TransformOut> {
    public name = "AirQualityStationsTransformation";

    protected transformInternal = (data: TransformIn): TransformOut => {
        const res: TransformOut = {
            indexes: [],
            measurements: [],
            stations: [],
        };

        for (const state of data.States) {
            const dateFrom = dateTime(new Date(state.DateFromUTC.replace("UTC", "+0000"))).toISOString({ includeMillis: true });
            const dateTo = dateTime(new Date(state.DateToUTC.replace("UTC", "+0000"))).toISOString({ includeMillis: true });

            for (const region of state.Regions) {
                for (const station of region.Stations) {
                    if (station.Code !== "") {
                        // station identifier
                        const id = `${state.Code}_${region.Code}_${station.Code}`;

                        // stations
                        res.stations.push({
                            classification: station.Classif ?? null,
                            id,
                            latitude: station.Lat ? parseFloat(station.Lat) : null,
                            longitude: station.Lon ? parseFloat(station.Lon) : null,
                            owner: station.Owner ?? null,
                            region_code: region.Code,
                            region_name: region.Name,
                            state_code: state.Code,
                            state_name: state.Name,
                            station_name: station.Name,
                            station_vendor_id: station.Code,
                        });

                        // indexes
                        if (station.Ix) {
                            res.indexes.push({
                                index_code: station.Ix,
                                measured_from: dateFrom,
                                measured_to: dateTo,
                                station_id: id,
                            });
                        }

                        // measurements
                        if (station.Components) {
                            for (const component of station.Components) {
                                if (
                                    (component.Flag && (component.Flag === "ok" || component.Flag === "no_data")) ||
                                    (component.Val && !component.Flag)
                                ) {
                                    res.measurements.push({
                                        aggregation_interval: component.Int,
                                        component_code: component.Code,
                                        measured_from: dateFrom,
                                        measured_to: dateTo,
                                        station_id: id,
                                        value: component.Val // Flag no_data
                                            ? parseFloat(component.Val)
                                            : null,
                                    });
                                }
                            }
                        }
                    }
                }
            }
        }

        return res;
    };
}
