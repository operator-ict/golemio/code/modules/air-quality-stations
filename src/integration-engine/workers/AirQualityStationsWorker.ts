import { IEModuleContainer } from "#ie/ioc/Di";
import { IEModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { Refresh1HDataInDBTask } from "#ie/workers/tasks/Refresh1HDataInDBTask";
import { Refresh3HDataInDBTask } from "#ie/workers/tasks/Refresh3HDataInDBTask";
import { UpdateDistrictTask } from "#ie/workers/tasks/UpdateDistrictTask";
import { AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { WORKER_NAME } from "./constants";

export class AirQualityStationsWorker extends AbstractWorker {
    protected name = WORKER_NAME;

    constructor() {
        super();

        // Register tasks
        this.registerTask(IEModuleContainer.resolve<Refresh1HDataInDBTask>(IEModuleContainerToken.Refresh1HDataInDBTask));
        this.registerTask(IEModuleContainer.resolve<Refresh3HDataInDBTask>(IEModuleContainerToken.Refresh3HDataInDBTask));
        this.registerTask(IEModuleContainer.resolve<UpdateDistrictTask>(IEModuleContainerToken.UpdateDistrictTask));
    }
}
