import { IChmiInput } from "#sch/datasources/interfaces/IChmiInput";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { IDataSource, ProtocolStrategy } from "@golemio/core/dist/integration-engine";
import { DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine/datasources";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { chmiDatasourceJsonSchema } from "#sch/datasources/chmiDatasourceJsonSchema";

@injectable()
export class Datasource3HData {
    private static DATASOURCE_NAME = "Datasource3HData";

    constructor(@inject(CoreToken.SimpleConfig) private config: ISimpleConfig) {}

    public getDataSource(): IDataSource<IChmiInput> {
        return new DataSource<IChmiInput>(
            Datasource3HData.DATASOURCE_NAME,
            this.getProtocolStrategy(),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(Datasource3HData.DATASOURCE_NAME, chmiDatasourceJsonSchema)
        );
    }

    private getProtocolStrategy(): ProtocolStrategy {
        return new HTTPFetchProtocolStrategy({
            headers: {},
            method: "GET",
            url: this.config.getValue<string>("module.AirQualityStations3H"),
        });
    }
}
