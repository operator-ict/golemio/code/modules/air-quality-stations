import { PG_SCHEMA } from "#sch/constants";
import { AirQualityStationModel } from "#sch/models";
import { PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class StationsRepository extends PostgresModel {
    constructor() {
        super(
            "StationsRepository",
            {
                outputSequelizeAttributes: AirQualityStationModel.attributeModel,
                pgTableName: AirQualityStationModel.tableName,
                pgSchema: PG_SCHEMA,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("StationsRepositoryValidator", AirQualityStationModel.jsonSchema)
        );
    }
}
