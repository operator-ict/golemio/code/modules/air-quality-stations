import { PG_SCHEMA } from "#sch/constants";
import { AirQualityIndexModel } from "#sch/models";
import { PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class IndexesRepository extends PostgresModel {
    constructor() {
        super(
            "IndexesRepository",
            {
                outputSequelizeAttributes: AirQualityIndexModel.attributeModel,
                pgTableName: AirQualityIndexModel.tableName,
                pgSchema: PG_SCHEMA,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("IndexesRepositoryValidator", AirQualityIndexModel.jsonSchema)
        );
    }
}
