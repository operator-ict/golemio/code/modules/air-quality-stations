import { PG_SCHEMA } from "#sch/constants";
import { AirQualityMeasurementModel } from "#sch/models";
import { PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class MeasurementsRepository extends PostgresModel {
    constructor() {
        super(
            "MeasurementsRepository",
            {
                outputSequelizeAttributes: AirQualityMeasurementModel.attributeModel,
                pgTableName: AirQualityMeasurementModel.tableName,
                pgSchema: PG_SCHEMA,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("MeasurementsRepositoryValidator", AirQualityMeasurementModel.jsonSchema)
        );
    }
}
