import { PG_SCHEMA } from "#sch/constants";
import { AirQualityIndexModel } from "#sch/models/AirQualityIndexModel";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { IBaseModel } from "@golemio/core/dist/output-gateway/models/interfaces/IBaseModel";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import IHistoryFilterParameters from "../../schema-definitions/models/interfaces/IHistoryFilterParams";
import { AirQualityMeasurementsRepository } from "./AirQualityMeasurementsRepository";
import { AirQualityStationsRepository } from "./AirQualityStationsRepository";
import HistoryRepositoryHelper from "./helpers/HistoryRepositoryHelper";
import OutputDtoMapper from "./helpers/OutputDtoMapper";
import { ParamHelper } from "./helpers/ParamHelper";

@injectable()
export class AirQualityHistoryRepository extends SequelizeModel implements IBaseModel {
    private stationModel: AirQualityStationsRepository;
    private measurementsModel: AirQualityMeasurementsRepository;

    constructor() {
        super("AirQualityHistoryRepository", AirQualityIndexModel.tableName, AirQualityIndexModel.attributeModel, {
            schema: PG_SCHEMA,
        });

        this.stationModel = new AirQualityStationsRepository();
        this.measurementsModel = new AirQualityMeasurementsRepository();

        this.sequelizeModel.hasOne(this.stationModel.sequelizeModel, {
            sourceKey: "station_id",
            foreignKey: "id",
            as: "station",
        });

        this.sequelizeModel.hasMany(this.measurementsModel.sequelizeModel, {
            sourceKey: "station_id",
            foreignKey: "station_id",
            as: "measurements",
        });
    }

    public GetAll = async (options?: IHistoryFilterParameters): Promise<any> => {
        const result = await this.sequelizeModel.findAll<AirQualityIndexModel>({
            attributes: {
                include: ["updated_at"],
            },
            include: [
                {
                    model: this.stationModel.sequelizeModel,
                    attributes: {
                        include: ["updated_at"],
                    },
                    as: "station",
                    required: true,
                    where: {
                        [Sequelize.Op.and]: [
                            { region_code: "A" }, // filter Prague region only
                            ...HistoryRepositoryHelper.filterByStationId(options),
                        ],
                    },
                },
                {
                    model: this.measurementsModel.sequelizeModel,
                    where: Sequelize.literal(
                        `airqualitystations_indexes.measured_from = measurements.measured_from
                        and airqualitystations_indexes.measured_to = measurements.measured_to`
                    ),
                    as: "measurements",
                },
            ],
            where: options
                ? {
                      [Sequelize.Op.and]: [
                          ...HistoryRepositoryHelper.filterByMeasuredFrom(options),
                          ...HistoryRepositoryHelper.filterByMeasuredTo(options),
                      ],
                  }
                : {},
            limit: ParamHelper.getPaginationLimit(options?.limit),
            offset: Number.isInteger(options?.offset) ? options?.offset : 0,
        });

        return result.map((element) => OutputDtoMapper.mapToHistoricItem(element));
    };

    public GetOne(): Promise<never> {
        throw new Error("Method not implemented.");
    }
}
