import { PG_SCHEMA } from "#sch/constants";
import { AirQualityComponentType } from "#sch/models/AirQualityComponentType";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class AirQualityComponentTypesRepository extends SequelizeModel {
    constructor() {
        super(
            "AirQualityComponentTypesRepository",
            "airqualitystations_component_types",
            AirQualityComponentType.attributeModel,
            {
                schema: PG_SCHEMA,
            }
        );
    }

    public GetAll = (): Promise<AirQualityComponentType[]> => {
        return this.sequelizeModel.findAll<AirQualityComponentType>();
    };

    GetOne(): Promise<never> {
        throw new Error("Method not implemented.");
    }
}
