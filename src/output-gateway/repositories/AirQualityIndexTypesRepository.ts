import { PG_SCHEMA } from "#sch/constants";
import { AirQualityComponentType } from "#sch/models/AirQualityComponentType";
import { AirQualityIndexType } from "#sch/models/AirQualityIndexType";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class AirQualityIndexTypesRepository extends SequelizeModel {
    constructor() {
        super("AirQualityIndexTypesRepository", "airqualitystations_index_types", AirQualityIndexType.attributeModel, {
            schema: PG_SCHEMA,
        });
    }

    public GetAll = (): Promise<AirQualityComponentType[]> => {
        return this.sequelizeModel.findAll<AirQualityComponentType>();
    };

    GetOne(): Promise<never> {
        throw new Error("Method not implemented.");
    }
}
