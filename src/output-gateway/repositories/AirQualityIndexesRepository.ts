import { buildIndexesQuery } from "#og/repositories/helpers/RawQueryProvider";
import { PG_SCHEMA } from "#sch/constants";
import { AirQualityIndexModel } from "#sch/models/AirQualityIndexModel";
import IAirQualityIndex from "#sch/models/interfaces/IAirQualityIndex";
import {
    buildGeojsonFeatureCollection,
    IGeoJsonAllFilterParameters,
    IGeoJSONFeature,
    SequelizeModel,
} from "@golemio/core/dist/output-gateway";
import { IGeoJsonModel } from "@golemio/core/dist/output-gateway/models/interfaces/IGeoJsonModel";
import { QueryTypes } from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import OutputDtoMapper from "./helpers/OutputDtoMapper";
import { ParamHelper } from "./helpers/ParamHelper";

@injectable()
export class AirQualityIndexesRepository extends SequelizeModel implements IGeoJsonModel {
    constructor() {
        super("AirQualityIndexesRepository", AirQualityIndexModel.tableName, AirQualityIndexModel.attributeModel, {
            schema: PG_SCHEMA,
        });
    }

    public GetAll = async (options?: IGeoJsonAllFilterParameters): Promise<any> => {
        const result = await this.sequelizeModel.sequelize?.query<AirQualityIndexModel>(buildIndexesQuery(options), {
            type: QueryTypes.SELECT,
            nest: true,
            raw: true,
            replacements: {
                districts: options?.districts,
                updatedAt: options?.updatedSince,
                longitude: options?.lng,
                latitude: options?.lat,
                range: options?.range,
                limit: ParamHelper.getPaginationLimit(options?.limit),
                offset: Number.isInteger(options?.offset) ? options?.offset : 0,
            },
        });

        return this.formatResult(result);
    };

    GetOne(): Promise<never> {
        throw new Error("Method not implemented.");
    }

    GetProperties = (): never => {
        throw new Error("Method not implemented.");
    };

    //backwards compatibility
    IsPrimaryIdNumber(idKey: string): Promise<boolean> {
        return Promise.resolve(false);
    }

    PrimaryIdentifierSelection(arg0: string): object {
        return { station_id: arg0 };
    }

    public formatResult = (result: AirQualityIndexModel[] | IAirQualityIndex[] | undefined) => {
        if (!result) {
            return buildGeojsonFeatureCollection([]);
        }

        const featureItems: Array<IGeoJSONFeature | undefined> = result.map((element: any) =>
            OutputDtoMapper.mapToFeatureItem(element)
        );
        const nonEmptyItems = featureItems.filter((item) => item !== undefined) as IGeoJSONFeature[];
        return buildGeojsonFeatureCollection(nonEmptyItems);
    };
}
