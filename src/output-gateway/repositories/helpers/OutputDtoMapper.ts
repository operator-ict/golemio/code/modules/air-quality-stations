import IAirQualityIndex from "#sch/models/interfaces/IAirQualityIndex";
import IAirQualityMeasurement from "#sch/models/interfaces/IAirQualityMeasurement";
import { IGeoJSONFeature, TGeoCoordinates } from "@golemio/core/dist/output-gateway";

export default class OutputDtoMapper {
    public static mapToHistoricItem = (result: IAirQualityIndex): any | undefined => {
        if (!result || !result.station || !result.measurements) return undefined;

        let output = {} as any;
        output.id = result.station.station_vendor_id;
        output.measurement = OutputDtoMapper.mapToMeasurementDto(result.index_code, result.measurements);
        output.updated_at = result.updated_at;

        return output;
    };

    public static mapToFeatureItem = (result: IAirQualityIndex): IGeoJSONFeature | undefined => {
        if (!result || !result.station || !result.measurements) return undefined;

        let output = {} as any;
        output.geometry = {} as TGeoCoordinates;
        output.geometry.coordinates = [result.station.longitude, result.station.latitude];
        output.geometry.type = "Point";
        output.properties = {} as any;
        output.properties.measurement = OutputDtoMapper.mapToMeasurementDto(result.index_code, result.measurements);
        output.properties.id = result.station.station_vendor_id;
        output.properties.name = result.station.station_name;
        output.properties.updated_at = result.station.updated_at;
        output.properties.district = result.station.district;
        output.type = "Feature";

        return output;
    };

    private static mapToMeasurementDto(indexCode: string, measurements: IAirQualityMeasurement[]) {
        let measurement = {} as any;
        measurement.AQ_hourly_index = indexCode;
        measurement.components = measurements
            .filter((element) => element.value) // do not show null values
            .map((element) => {
                return {
                    averaged_time: {
                        // atm we get intervals only 1h, 3h, 24h, removes h at the end
                        averaged_hours: element.aggregation_interval.endsWith("h")
                            ? element.aggregation_interval.slice(0, -1)
                            : element.aggregation_interval,
                        value: element.value,
                    },
                    type: element.component_code,
                };
            });
        return measurement;
    }
}
