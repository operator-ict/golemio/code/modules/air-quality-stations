import IHistoryFilterParameters from "#sch/models/interfaces/IHistoryFilterParams";
import { Op, WhereOptions } from "@golemio/core/dist/shared/sequelize";

export default class HistoryRepositoryHelper {
    public static filterByMeasuredFrom(options: IHistoryFilterParameters): WhereOptions[] {
        return options.from
            ? [
                  {
                      measured_from: { [Op.gte]: options.from },
                  },
              ]
            : [];
    }

    public static filterByMeasuredTo(options: IHistoryFilterParameters): WhereOptions[] {
        return options.to
            ? [
                  {
                      measured_to: { [Op.lt]: options.to },
                  },
              ]
            : [];
    }

    public static filterByStationId(options: IHistoryFilterParameters | undefined): WhereOptions[] {
        return options?.sensorId
            ? [
                  {
                      station_vendor_id: options.sensorId,
                  },
              ]
            : [];
    }
}
