import { config } from "@golemio/core/dist/output-gateway/config";

export class ParamHelper {
    public static getPaginationLimit(queryLimit: number | undefined) {
        return queryLimit != undefined &&
            Number.isInteger(queryLimit) &&
            queryLimit < config.pagination_max_limit &&
            queryLimit >= 0
            ? queryLimit
            : config.pagination_max_limit;
    }
}
