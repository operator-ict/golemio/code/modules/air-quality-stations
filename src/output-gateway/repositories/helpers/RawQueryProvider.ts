import { IGeoJsonAllFilterParameters } from "@golemio/core/dist/output-gateway";
import { PG_SCHEMA } from "#sch/constants";

const allIndexes = `
select
	"aqi"."index_code",
	"aqi"."measured_from",
	"aqi"."measured_to",
	"aqi"."station_id",
	"aqi"."updated_at",
	"stations"."classification" as "station.classification",
	"stations"."id" as "station.id",
	"stations"."latitude" as "station.latitude",
	"stations"."longitude" as "station.longitude",
	"stations"."owner" as "station.owner",
	"stations"."region_code" as "station.region_code",
	"stations"."region_name" as "station.region_name",
	"stations"."state_code" as "station.state_code",
	"stations"."state_name" as "station.state_name",
	"stations"."station_name" as "station.station_name",
	"stations"."station_vendor_id" as "station.station_vendor_id",
	"stations"."district" as "station.district",
	"stations"."create_batch_id" as "station.create_batch_id",
	"stations"."created_at" as "station.created_at",
	"stations"."created_by" as "station.created_by",
	"stations"."update_batch_id" as "station.update_batch_id",
	"stations"."updated_at" as "station.updated_at",
	"stations"."updated_by" as "station.updated_by",
    (SELECT json_agg(item) FROM (
        select component_code, aggregation_interval, value
        from ${PG_SCHEMA}.airqualitystations_measurements am
        where am.station_id = "currentData".station_id
            and am.measured_from = "currentData".measured_from
            and am.measured_to = "currentData".measured_to
        ) item
    ) AS "measurements"
    |additionalColumns|
from
	(select
		station_id,
		max(measured_from) as "measured_from",
		max(measured_to) as "measured_to"
	from
	${PG_SCHEMA}.airqualitystations_indexes ai
	where
		station_id in (select id from ${PG_SCHEMA}.airqualitystations a where a.region_code = 'A')
	group by station_id) as "currentData"
inner join ${PG_SCHEMA}.airqualitystations as "stations" on
	"currentData"."station_id" = "stations"."id"
inner join ${PG_SCHEMA}.airqualitystations_indexes as "aqi" on
	"currentData"."station_id" = "aqi"."station_id"
	and "currentData"."measured_from" = "aqi"."measured_from"
	and "currentData"."measured_to" = "aqi"."measured_to"`;

const whereUpdateAt = `"stations"."updated_at" > :updatedAt`;
const whereInRange = `(ST_DWithin(
    ST_SetSRID(ST_MakePoint("stations"."longitude","stations"."latitude"),	4326),
    cast(ST_SetSRID(ST_MakePoint(:longitude, :latitude), 4326) as GEOGRAPHY),
    :range
))`;
const whereDistrict = `"stations"."district" in (:districts)`;
const distanceColumn = `,ST_Distance(
    ST_SetSRID(ST_MakePoint("stations"."longitude","stations"."latitude"),4326),
    cast(ST_SetSRID(ST_MakePoint(:longitude, :latitude), 4326) as GEOGRAPHY)
) as "station.distance"`;
const orderBy = ` order by "station.distance" `;
const footerLimit = ` limit :limit`;
const footerOffset = ` offset :offset`;

export const buildIndexesQuery = (options: IGeoJsonAllFilterParameters | undefined): string => {
    let preparedQuery = allIndexes;
    preparedQuery = allIndexes.replace("|additionalColumns|", options?.lng && options?.lat ? distanceColumn : "");

    let whereConditions = [];
    if (options?.updatedSince) whereConditions.push(whereUpdateAt);
    if (options?.lng && options.lat && options.range) whereConditions.push(whereInRange);
    if (options?.districts) whereConditions.push(whereDistrict);

    preparedQuery = preparedQuery
        .concat(...(whereConditions.length > 0 ? [" where ", whereConditions.join(" and ")] : ""))
        .concat(Number.isFinite(options?.lng) && Number.isFinite(options?.lat) ? orderBy : "")
        .concat(Number.isInteger(options?.limit) ? footerLimit : "")
        .concat(Number.isInteger(options?.offset) ? footerOffset : "");

    return preparedQuery;
};
