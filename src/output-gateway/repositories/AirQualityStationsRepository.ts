import { PG_SCHEMA } from "#sch/constants";
import { AirQualityStationModel } from "#sch/models/AirQualityStationModel";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class AirQualityStationsRepository extends SequelizeModel {
    constructor() {
        super("AirQualityStationsRepository", AirQualityStationModel.tableName, AirQualityStationModel.attributeModel, {
            schema: PG_SCHEMA,
        });
    }

    GetAll = async (): Promise<never> => {
        throw new Error("Method not implemented.");
    };

    GetOne(): Promise<never> {
        throw new Error("Method not implemented.");
    }
}
