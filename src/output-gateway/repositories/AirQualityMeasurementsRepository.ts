import { PG_SCHEMA } from "#sch/constants";
import { AirQualityMeasurementModel } from "#sch/models/AirQualityMeasurementModel";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class AirQualityMeasurementsRepository extends SequelizeModel {
    constructor() {
        super(
            "AirQualityMeasurementsRepository",
            AirQualityMeasurementModel.tableName,
            AirQualityMeasurementModel.attributeModel,
            {
                schema: PG_SCHEMA,
            }
        );
    }

    GetAll(): Promise<never> {
        throw new Error("Method not implemented.");
    }

    GetOne(): Promise<never> {
        throw new Error("Method not implemented.");
    }
}
