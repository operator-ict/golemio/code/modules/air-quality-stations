import { OGModuleContainer } from "#og/ioc/Di";
import { OGModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { GeoJsonRouter } from "@golemio/core/dist/output-gateway";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import AirQualityHistoryRouter from "./AirQualityHistoryRouter";
import { AirQualityComponentTypesRepository } from "#og/repositories/AirQualityComponentTypesRepository";
import { AirQualityIndexesRepository } from "#og/repositories/AirQualityIndexesRepository";
import { AirQualityIndexTypesRepository } from "#og/repositories/AirQualityIndexTypesRepository";

const CACHE_MAX_AGE = 20 * 60;
const CACHE_STALE_WHILE_REVALIDATE = 5 * 60;

export default class AirQualityRouter extends GeoJsonRouter {
    public router: Router = Router();
    private airQualityComponentTypes: AirQualityComponentTypesRepository;
    private airQualityIndexTypes: AirQualityIndexTypesRepository;

    constructor() {
        super(new AirQualityIndexesRepository());
        this.initRoutes({ maxAge: CACHE_MAX_AGE, staleWhileRevalidate: CACHE_STALE_WHILE_REVALIDATE });
        this.router.get(
            "/indextypes",
            this.cacheHeaderMiddleware.getMiddleware(CACHE_MAX_AGE, CACHE_STALE_WHILE_REVALIDATE),
            this.GetIndexTypes
        );
        this.router.get(
            "/componenttypes",
            this.cacheHeaderMiddleware.getMiddleware(CACHE_MAX_AGE, CACHE_STALE_WHILE_REVALIDATE),
            this.GetComponentTypes
        );

        this.airQualityComponentTypes = new AirQualityComponentTypesRepository();
        this.airQualityIndexTypes = new AirQualityIndexTypesRepository();

        const historyRouter = new AirQualityHistoryRouter(
            OGModuleContainer.resolve(OGModuleContainerToken.AirQualityHistoryRepository)
        );
        historyRouter.initRoutes();
        this.router.use("/history", historyRouter.router);
    }

    public GetIndexTypes = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.airQualityIndexTypes.GetAll();
            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    public GetComponentTypes = async (req: Request, res: Response, next: any) => {
        try {
            const data = await this.airQualityComponentTypes.GetAll();
            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };
}

const airQualityRouter: Router = new AirQualityRouter().router;

export { airQualityRouter };
