import { OGModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { AirQualityComponentTypesRepository } from "#og/repositories/AirQualityComponentTypesRepository";
import { AirQualityHistoryRepository } from "#og/repositories/AirQualityHistoryRepository";
import { AirQualityIndexesRepository } from "#og/repositories/AirQualityIndexesRepository";
import { AirQualityIndexTypesRepository } from "#og/repositories/AirQualityIndexTypesRepository";
import { AirQualityMeasurementsRepository } from "#og/repositories/AirQualityMeasurementsRepository";
import { AirQualityStationsRepository } from "#og/repositories/AirQualityStationsRepository";
import { OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc/Di";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";

//#region Initialization
const OGModuleContainer: DependencyContainer = OutputGatewayContainer.createChildContainer();
//#endregion

//#region Repositories
OGModuleContainer.registerSingleton(OGModuleContainerToken.AirQualityComponentTypesRepository, AirQualityComponentTypesRepository)
    .registerSingleton(OGModuleContainerToken.AirQualityHistoryRepository, AirQualityHistoryRepository)
    .registerSingleton(OGModuleContainerToken.AirQualityIndexesRepository, AirQualityIndexesRepository)
    .registerSingleton(OGModuleContainerToken.AirQualityIndexTypesRepository, AirQualityIndexTypesRepository)
    .registerSingleton(OGModuleContainerToken.AirQualityMeasurementsRepository, AirQualityMeasurementsRepository)
    .registerSingleton(OGModuleContainerToken.AirQualityStationsRepository, AirQualityStationsRepository);
//#endregion

export { OGModuleContainer };
