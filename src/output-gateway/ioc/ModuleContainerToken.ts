const OGModuleContainerToken = {
    /* repositories */
    AirQualityComponentTypesRepository: Symbol(),
    AirQualityHistoryRepository: Symbol(),
    AirQualityIndexesRepository: Symbol(),
    AirQualityIndexTypesRepository: Symbol(),
    AirQualityMeasurementsRepository: Symbol(),
    AirQualityStationsRepository: Symbol(),
};

export { OGModuleContainerToken };
