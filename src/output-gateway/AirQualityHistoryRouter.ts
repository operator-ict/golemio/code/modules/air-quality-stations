import { OGModuleContainer } from "#og/ioc/Di";
import { OGModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { AirQualityHistoryRepository } from "#og/repositories/AirQualityHistoryRepository";
import { dateTime } from "@golemio/core/dist/helpers";
import { CacheHeaderMiddleware, checkErrors, pagination, paginationLimitMiddleware } from "@golemio/core/dist/output-gateway";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { BaseRouter } from "@golemio/core/dist/output-gateway/routes/BaseRouter";
import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { query, ValidationChain } from "@golemio/core/dist/shared/express-validator";
import { inject } from "@golemio/core/dist/shared/tsyringe";

export default class AirQualityHistoryRouter extends BaseRouter {
    protected cacheHeaderMiddleware: CacheHeaderMiddleware;

    constructor(
        @inject(OGModuleContainerToken.AirQualityHistoryRepository) private historyRepository: AirQualityHistoryRepository
    ) {
        super();
        this.cacheHeaderMiddleware = OGModuleContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);
    }

    public initRoutes = async () => {
        const sensorIdParam = await this.GetIdQueryParamWithCorrectType();
        this.router.get(
            "/",
            [
                query("from").optional().isISO8601().not().isArray(),
                query("to").optional().isISO8601().not().isArray(),
                sensorIdParam,
            ],
            pagination,
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(20 * 60, 5 * 60),
            paginationLimitMiddleware("AirQualityHistoryRouter"),
            this.GetAll
        );
    };

    public GetAll = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.historyRepository.GetAll({
                sensorId: req.query.sensorId as string,
                from: (req.query.from as string) ?? dateTime(new Date()).subtract(1, "days").toISOString(),
                to: (req.query.to as string) ?? dateTime(new Date()).toISOString(),
                limit: req.query.limit ? parseInt(req.query.limit as string) : undefined,
                offset: req.query.offset ? parseInt(req.query.offset as string) : undefined,
            });

            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    protected GetIdQueryParamWithCorrectType = async (): Promise<ValidationChain> => {
        return query("sensorId").optional().not().isEmpty({ ignore_whitespace: true }).not().isArray();
    };
}
